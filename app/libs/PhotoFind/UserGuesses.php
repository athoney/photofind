<?php

namespace PhotoFind;

class UserGuesses
{
    protected $f3;
    protected $db;
    protected $files;
    
    public function __construct()
    {
        $this->f3 = \Base::instance();
        $this->db = $this->f3->get("DB");
    }

    public function recordGuess(int $questId, int $photoId, string $name, string $username, string $email, \PhotoFind\Files $files):bool {
        
        $guessMapper = new \DB\SQL\Mapper($this->db,'guesses');
        
        $guessMapper->questId = $questId;
        $guessMapper->photoId = $photoId;
		$guessMapper->name = $name;
		$guessMapper->username = $username;
		$guessMapper->email = $email;
		$guessMapper->active = 1;
        $guessMapper->submitDate = date('Y-m-d h:i:s');
		$guessMapper->save();
		
		$uploadedPhotoPath = $files->storeGuess($questId, $guessMapper->Id);
        if (!empty($uploadedPhotoPath)) {
            $guessMapper->fileLocation = $uploadedPhotoPath;
            $guessMapper->save();
        }

        return true;		
    }

}
