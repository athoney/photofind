<?php

namespace PhotoFind;

class Prizes
{
    protected $f3;
    protected $db;
    protected $files;
    
    public function __construct()
    {
        $this->f3 = \Base::instance();
        $this->db = $this->f3->get("DB");
        $this->files = new \PhotoFind\Files();
    }
    
    public function getPrize(int $prizeId):array {
        $prize = [];
        if (empty($prizeId)) {
            return $prize;
        }
        $prizeMapper = new \DB\SQL\Mapper($this->db,'prizes');
		$prizeMapper->load(['id = ?',$prizeId]);
		if (!$prizeMapper->dry()) {
		    $prize = $prizeMapper->cast();
		}
		return $prize;
    }
    
    public function getPrizes() {
		$prizesMapper = new \DB\SQL\Mapper($this->db,'prizes');
		$prizesList = $prizesMapper->find();
		$prizesListArray = [];
		foreach($prizesList as $prize) {
		    $prizesListArray[] = $prize->cast();
		}
		return $prizesListArray;
    }
    
    public function getAvailablePrizes() {
		$sql = "select prizes.* from prizes where prizes.id not in (select prizeId from photos where prizeId is not null)";
		$prizesList = $this->db->exec($sql, [], 1);
		return $prizesList;
    }
    
    public function updatePrize(array $prizeData):array {
        
        $prizeId = $prizeData['id']??0;
        
        $prizeMapper = new \DB\SQL\Mapper($this->db,'prizes');
        if (!empty($prizeId))  {
            $prizeMapper->load(['id = ?',$prizeId]);
			if ($prizeMapper->dry()) {
				$prizeMapper->reset();
			}
        }

		$prizeMapper->name = $prizeData['name'];
		$prizeMapper->description = $prizeData['description'];
		$prizeMapper->photoId = empty($prizeData['photoId'])?NULL:$prizeData['photoId'];
        
		$prizeMapper->save();
		
        // after photo record successfully created, store uploaded 
        // photo if present.
        if (!empty($prizeMapper->id)) {
            $uploadedPhotoPath = $this->files->storePrize($prizeMapper->id);
            if (!empty($uploadedPhotoPath)) {
                if (!empty($prizeMapper->image)) {
                    $this->files->deleteFile($prizeMapper->image);
                }
                $prizeMapper->image = $uploadedPhotoPath;
                $prizeMapper->save();
            }
        }
        
        return $prizeMapper->cast();
    }
}