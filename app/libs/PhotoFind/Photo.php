<?php

namespace PhotoFind;

class Photo extends ModelBase {

    protected $f3;
    protected $db;
    protected $files;
    
    public function __construct()
    {
        $this->f3 = \Base::instance();
        $this->db = $this->f3->get("DB");
        $this->files = new \PhotoFind\Files();
    }
    
    public function getPhoto(int $photoId):array {
        $photo = [];
        if (empty($photoId)) {
            return $photo;
        }
        $photoMapper = new \DB\SQL\Mapper($this->db,'photos');
		$photoMapper->load(['id = ?',$photoId]);
		if (!$photoMapper->dry()) {
		    $photo = $photoMapper->cast();
		}
		return $photo;
    }
    
     public function getQuestPhotos(int $questId):array {
        $photoMapper = new \DB\SQL\Mapper($this->db,'photo');
		$photoMapper->load(['questId = ?',$questId]);
		$files = [];
		while (!$photoMapper->dry()) {
		    if ($photoMapper->type == 'questphoto') {
		        if (empty($files[$photoMapper->type])) {
                    $files[$photoMapper->type] = [];
		        }
		        $files[$photoMapper->type][] = $photoMapper->cast();
		    }
		    else {
		        $files[$photoMapper->type] = $photoMapper->cast();
		    }
		    $photoMapper->next();
		}
		
		$photoIds = [];
		if (!empty($files['questphoto'])) {
		    foreach ($files['questphoto'] as $key => $info) {
		        $photoIds[$info['Id']] = $key;
		    }
		}
		
		$files['questphotolist'] = $photoIds;
        return $files;
    }

    public function deleteAllPhotosByQuestId(int $questId):bool {
        $photoMapper = new \DB\SQL\Mapper($this->db,'photo');
		$photoMapper->load(['questId = ?',$questId]);
		$files = new \PhotoFind\Files();
		while (!$photoMapper->dry()) {
		    $files->deleteFile($photoMapper->fileLocation);
		    $photoMapper->next();
		}
		$photoMapper->erase(['questId = ?',$questId]);
        return true;
    }
   
    public function deletePhoto(int $photoId):bool {
        $photoMapper = new \DB\SQL\Mapper($this->db,'photo');
        if (!empty($photoId)) {
            $photoMapper->load(['Id = ?',$photoId]);
        }

        // TODO: consider not deleting photos if associated with guesses.
        
        if (!$photoMapper->dry()) {
            // delete image from drive
            $this->files->deleteFile($photoMapper->fileLocation??'');
            $this->files->deleteFile($photoMapper->fileLocationCrop??'');
            $this->files->deleteFile($photoMapper->fileLocationWeb??'');
            $photoMapper->erase();
            return true;
        }
        else {
            return false;
        }
    }

    private function storePhoto(int $photoId, int $questId, string $type, string $file, string $fileCrop = '', string $fileWeb = ''):bool {
        $photoMapper = new \DB\SQL\Mapper($this->db,'photo');
        if (!empty($photoId)) {
            $photoMapper->load(['Id = ?',$photoId]);
        }
        
        if (!$photoMapper->dry()) {
            // replacing image, so delete image from drive
            $this->files->deleteFile($photoMapper->fileLocation??'');
        }
        
        $photoMapper->questId          = $questId;
        $photoMapper->type             = $type;
        $photoMapper->fileLocation     = $file;
        $photoMapper->fileLocationCrop = $fileCrop??null;
        $photoMapper->fileLocationWeb  = $fileWeb??null;
        $photoMapper->save();
        
        return true;
    }
    
    public function storePrizePhoto(int $photoId, int $userId, int $questId, string $file):bool {
        $file = $this->f3->get('BASEDIR') . '/' . $file;
        $targetPhotoPath = '/accounts/u' . $userId . '/q' . $questId . '/prize-' . $this::getRandomString(10) . '.' . strtolower(pathinfo($file,PATHINFO_EXTENSION));
        $fileStored = $this->files->storeFile($file, $targetPhotoPath);
        if ($fileStored) {
            $this->files->resizePhoto($targetPhotoPath, "800x600");
            $this->storePhoto($photoId, $questId, 'prize', $targetPhotoPath);
        }
        return true;
    }
    
    public function storeQuestPhoto(int $photoId, int $userId, int $questId, string $origFilePath, string $cropFileData):bool {
        $timestamp = time();

        $origFilePath = $this->f3->get('BASEDIR') . '/' . $origFilePath;
        $cropFilePath = $this->f3->get('BASEDIR') . '/' . $this->f3->get('UPLOADS') . 'u' . $userId . '-q' . $questId . '-photo-' . $timestamp . '-crop.png';

        $targetPhotoPath     = '/accounts/u' . $userId . '/q' . $questId . '/photo-' . $timestamp . '-o-' . $this::getRandomString(10) . '.' . strtolower(pathinfo($origFilePath,PATHINFO_EXTENSION));
        $targetPhotoCropPath = '/accounts/u' . $userId . '/q' . $questId . '/photo-' . $timestamp . '-c-' . $this::getRandomString(10) . '.jpg';
        $targetWebPath       = '/accounts/u' . $userId . '/q' . $questId . '/photo-' . $timestamp . '-w-' . $this::getRandomString(10) . '.jpg';

        // Store original file
        $filesStored = $this->files->storeFile($origFilePath, $targetPhotoPath);
        // create a resized web version to be displayed on site
        $filesStored = $filesStored && $this->files->resizePhoto($targetPhotoPath, "1024x768", $targetWebPath);

        // store crop
        $imgBinary = base64_decode(str_replace('data:image/png;base64,','',$cropFileData));
        if ($imgBinary === false) {
            return false;
        }
        
        $targetPath = dirname($cropFilePath);
        if (!file_exists($targetPath)) {
            $oldmask = umask(0);
            mkdir($targetPath, 0777, true);
            umask($oldmask);
        }

        $fp = fopen($cropFilePath, 'w');
        fwrite($fp, $imgBinary);
        fclose($fp);
        $image = imagecreatefrompng($cropFilePath);
        imagejpeg($image, str_replace('.png','.jpg',$cropFilePath),100);
        imagedestroy($image);
        unlink($cropFilePath);
        $cropFilePath = str_replace('.png','.jpg',$cropFilePath);

        $filesStored = $filesStored && $this->files->storeFile($cropFilePath,$targetPhotoCropPath);

        if ($filesStored) {
            $this->storePhoto($photoId, $questId,'questphoto',$targetPhotoPath,$targetPhotoCropPath,$targetWebPath);
            return true;
        }
        else {
            $this->files->deleteFile($targetPhotoPath,true);
            $this->files->deleteFile($targetPhotoCropPath,true);
            return false;
        }
    }
    
}