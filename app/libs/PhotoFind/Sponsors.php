<?php

namespace PhotoFind;

class Sponsors
{
    protected $f3;
    protected $db;
    protected $files;
    
    public function __construct()
    {
        $this->f3 = \Base::instance();
        $this->db = $this->f3->get("DB");
        $this->files = new \PhotoFind\Files();
    }
    
    public function getSponsor(int $sponsorId):array {
        $sponsor = [];
        if (empty($sponsorId)) {
            return $sponsor;
        }
        $sponsorMapper = new \DB\SQL\Mapper($this->db,'sponsors');
		$sponsorMapper->load(['id = ?',$sponsorId]);
		if (!$sponsorMapper->dry()) {
		    $sponsor = $sponsorMapper->cast();
		}
		return $sponsor;
    }
    
    public function getSponsors(int $active):array {
		$sponsorsMapper = new \DB\SQL\Mapper($this->db,'sponsors');
		$sponsorsList = $sponsorsMapper->find();
		$sponsorsListArray = [];
		foreach($sponsorsList as $sponsor) {
		    $sponsorsListArray[] = $sponsor->cast();
		}
		return $sponsorsListArray;
    }
    /*
    public function getSponsor(int $sponsorId):array {
        // need to complete
        $sponsor = [];
        if (empty($sponsorId)){
            return $sponsor;
        }
        $sponsorMapper = new \DB\SQL\Mapper($this->db,'sponsors');
		$sponsorMapper->load(['id = ?',$sponsorId]);
		if (!$sponsorMapper->dry()) {
		    $sponsor = $sponsorMapper->cast();
		}
		return $sponsor;
    }
    */
    public function updateSponsor(array $sponsorData):array {
        
        $sponsorId = $sponsorData['id']??0;
        
        $sponsorMapper = new \DB\SQL\Mapper($this->db,'sponsors');
        if (!empty($sponsorId))  {
            $sponsorMapper->load(['id = ?',$sponsorId]);
			if ($sponsorMapper->dry()) {
				$sponsorMapper->reset();
			}
			else {
			    $sponsorData['dateUploaded'] = $sponsorMapper->dateUploaded;
			}            
        }
    	if (empty($sponsorData['dateUploaded'])) {
			$sponsorData['dateUploaded'] = date('Y-m-d H:i:s');
		}

		$sponsorMapper->name = $sponsorData['name'];
		$sponsorMapper->description = $sponsorData['description'];
		$sponsorMapper->address = $sponsorData['address'];
		$sponsorMapper->website = $sponsorData['website'];
		$sponsorMapper->pocName = $sponsorData['pocName'];
		$sponsorMapper->pocPhone = $sponsorData['pocPhone'];
		$sponsorMapper->pocEmail = $sponsorData['pocEmail'];
        $sponsorMapper->dateUploaded = $sponsorData['dateUploaded'];
        $sponsorMapper->active = $sponsorData['active']??0;
        
		$sponsorMapper->save();
		
        // after photo record successfully created, store uploaded 
        // photo if present.
        if (!empty($sponsorMapper->id)) {
            $uploadedPhotoPath = $this->files->storeSponsor($sponsorMapper->id);
            if (!empty($uploadedPhotoPath)) {
                if (!empty($sponsorMapper->logo)) {
                    $this->files->deleteFile($sponsorMapper->logo);
                }
                $sponsorMapper->logo = $uploadedPhotoPath;
                $sponsorMapper->save();
            }
        }
        
        return $sponsorMapper->cast();
    }
}