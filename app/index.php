<?php
ob_start();

require(__DIR__.'/init.php');

if (\F3::exists('secure') && \F3::get('secure') == '1' && empty($_SERVER['HTTPS'])) {
    header('Location: ' . \Wyolution\View::instance()->getBaseUrl());
    exit(0);
}

// Default route
$f3->route('GET /','controllers\Main->home'); //html
// $f3->route('GET /userLogin','controllers\Main->login'); //html
$f3->route('GET /howtoplay','controllers\Main->howToPlay'); //html
$f3->route('GET /winners','controllers\Main->winners'); //html
$f3->route('GET /quest/@questId','controllers\Main->quest'); //html
$f3->route('GET /photo/@photoId','controllers\Main->photo'); //html
// $f3->route('GET /hello','controllers\Main->helloWorld'); //html
$f3->route('GET /gallery','controllers\Main->galleryPhotos');
$f3->route('POST /guess','controllers\Main->record');
$f3->route('GET /tandc','controllers\Main->tandc');
$f3->route('GET /privacy','controllers\Main->privacy');

// Admin Routes
$f3->route('GET  /login','controllers\Authorize->login'); // html
$f3->route('POST /validate','controllers\Authorize->validate'); // html
$f3->route('GET  /logout','controllers\Authorize->logout'); // html
$f3->route('GET  /dashboard','controllers\AdminMain->dashboard');

$f3->route('GET  /admin-user','controllers\admin\User->showList'); // html
$f3->route('GET  /admin-user/@Id','controllers\admin\User->showUser'); // html
$f3->route('POST /admin-user/update','controllers\admin\User->updateUser'); // html

$f3->route('GET  /admin-quest','controllers\admin\Quest->showList'); // html
$f3->route('GET  /admin-quest/@Id','controllers\admin\Quest->showQuest'); // html
$f3->route('POST /admin-quest/update','controllers\admin\Quest->updateQuest'); // html

//guesses
//$f3->route('GET /admin/photoGuesses','controllers\AdminGuesses->getPhotos');
$f3->route('GET /admin-guess','controllers\admin\Guesses->getQuests');
$f3->route('GET /admin-guess/@questId','controllers\admin\Guesses->getQuestPhotos');
$f3->route('GET /admin-guess/review/@photoId','controllers\admin\Guesses->getPhotoGuesses');
$f3->route('POST /admin-guess/update','controllers\admin\Guesses->update');
$f3->route('POST /admin-guess/update/winner','controllers\admin\Guesses->updateWinner');
//questQuesses
$f3->route('GET /admin/questGuesses','controllers\AdminGuesses->getQuests');
$f3->route('GET /admin/questPhotos','controllers\AdminGuesses->getQuestPhotos');
$f3->route('GET /admin/photoGuesses','controllers\AdminGuesses->getPhotoGuesses');

$f3->route('GET /admin/audit','controllers\Admin->audit'); // html
$f3->route('GET /docs/release','controllers\Docs->releaseNotes');

$f3->route('GET /admin/photos','controllers\AdminPhotos->getPhotos');
$f3->route('GET /admin/photo','controllers\AdminPhotos->getPhoto');
$f3->route('GET /admin/photo/@id','controllers\AdminPhotos->getPhoto');
$f3->route('POST /admin/photo/update','controllers\AdminPhotos->update');
$f3->route('POST /admin/photo/delete','controllers\AdminPhotos->delete');
$f3->route('POST /admin/photo/changeStatus','controllers\AdminPhotos->changeStatus');

$f3->route('GET /admin/sponsors','controllers\AdminSponsors->getSponsors');
$f3->route('GET /admin/sponsor','controllers\AdminSponsors->getSponsor');
$f3->route('GET /admin/sponsor/@id','controllers\AdminSponsors->getSponsor');
$f3->route('POST /admin/sponsor/update','controllers\AdminSponsors->update');
$f3->route('POST /admin/sponsor/changeStatus','controllers\AdminSponsors->changeStatus');

$f3->route('GET /admin/prizes','controllers\AdminPrizes->getPrizes');
$f3->route('GET /admin/prize','controllers\AdminPrizes->getPrize');
$f3->route('GET /admin/prize/@id','controllers\AdminPrizes->getPrize');
$f3->route('POST /admin/prize/update','controllers\AdminPrizes->update');


// self help routes for a user who isn't authenticated
$f3->route('GET|POST /selfService/@task','controllers\SelfService->defaultRoute'); // html
$f3->route('GET /forgot','controllers\SelfService->forgot'); // html
$f3->route('POST /forgot/request','controllers\SelfService->requestReset'); // html
$f3->route('GET /forgot/reset','controllers\SelfService->resetPassword'); // html
$f3->route('POST /forgot/update','controllers\SelfService->resetPassword'); // html

// test routes
$f3->route('GET /test/smarty1','controllers\Test->smartyTestStep1'); //html
$f3->route('GET|POST /test/smarty2','controllers\Test->smartyTestStep2'); //html
$f3->route('GET /test/devLayout','controllers\Test->devLayout'); //html

// utility routes
$f3->route('GET /utils/session','controllers\Utils->session'); //html
$f3->route('GET /utils/hive','controllers\Utils->hive'); //html

// Show user-friendly error screen if something unexpected goes wrong
$f3->set('ONERROR',
    function($f3) {
        $utils = new controllers\Utils();
        $utils->error();
    }
);

$showDevOptions = $f3->get('SHOW_DEV_OPTIONS');
$f3->set('_showDevOptions', $showDevOptions);
$f3->run();?>