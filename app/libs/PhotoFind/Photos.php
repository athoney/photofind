<?php

namespace PhotoFind;

class Photos
{
    protected $f3;
    protected $db;
    protected $files;
    
    public function __construct()
    {
        $this->f3 = \Base::instance();
        $this->db = $this->f3->get("DB");
        $this->files = new \PhotoFind\Files();
    }
    
    public function getPhoto(int $photoId):array {
        $photo = [];
        if (empty($photoId)) {
            return $photo;
        }
        $photoMapper = new \DB\SQL\Mapper($this->db,'photos');
		$photoMapper->load(['id = ?',$photoId]);
		if (!$photoMapper->dry()) {
		    $photo = $photoMapper->cast();
		}
		return $photo;
    }
    
    public function getPhotos(int $status):array {
		$sql = "select photos.*, prizes.name AS prizeName, sponsors.name AS sponsorName
                FROM photos
                LEFT JOIN prizes ON prizes.id = photos.prizeId
                LEFT JOIN sponsors ON sponsors.id = photos.sponsorId";
        if ($status == 1) {
            $sql .= " where photos.active = 1";
        }
		$photoList = $this->db->exec($sql,[],1);
        return $photoList;
    }
    
    
    public function updatePhoto(array $photoData):array {
        
        $photoId = $photoData['id']??0;
        
        $photoMapper = new \DB\SQL\Mapper($this->db,'photos');
        if (!empty($photoId))  {
            $photoMapper->load(['id = ?',$photoId]);
			if ($photoMapper->dry()) {
				$photoMapper->reset();
			}
			else {
			    $photoData['dateUploaded'] = $photoMapper->dateUploaded;
			}            
        }
    	if (empty($photoData['dateUploaded'])) {
			$photoData['dateUploaded'] = date('Y-m-d H:i:s');
		}

		$photoMapper->name = $photoData['name'];
		$photoMapper->description = $photoData['description'];
        $photoMapper->dateUploaded = $photoData['dateUploaded'];
        if (in_array(intval($photoData['active']),[0,1,2])) {
            $photoMapper->active = intval($photoData['active']);
        }
        
        $photoMapper->hint1 = empty($photoData['hint1'])?NULL:$photoData['hint1'];
        $photoMapper->hint1Date = empty($photoData['hint1Date'])?NULL:date('Y-m-d',strtotime($photoData['hint1Date']));
        $photoMapper->hint2 = empty($photoData['hint2'])?NULL:$photoData['hint2'];
        $photoMapper->hint2Date = empty($photoData['hint2Date'])?NULL:date('Y-m-d',strtotime($photoData['hint2Date']));
        $photoMapper->hint3 = empty($photoData['hint3'])?NULL:$photoData['hint3'];
        $photoMapper->hint3Date = empty($photoData['hint3Date'])?NULL:date('Y-m-d',strtotime($photoData['hint3Date']));
        $photoMapper->prizeId = empty($photoData['prizeId'])?NULL:$photoData['prizeId'];
        $photoMapper->sponsorId = empty($photoData['sponsorId'])?NULL:$photoData['sponsorId'];
        
		$photoMapper->save();
		
        // after photo record successfully created, store uploaded 
        // photo if present.
        if (!empty($photoMapper->id)) {
            if (!empty($photoData['deletePhoto'])) {
                $this->files->deleteFile($photoMapper->fileLocation??'');
                $this->files->deleteFile($photoMapper->fileCropLocation??'');
                
                $photoMapper->fileLocation = '';
                $photoMapper->fileCropLocation = '';
                $photoMapper->save();
            }
            if ($photoMapper->fileLocation == '') {
                $uploadedPhotoPath = $this->files->storePhoto($photoMapper->id);
                if (!empty($uploadedPhotoPath)) {
                    $photoMapper->fileLocation = $uploadedPhotoPath;
                    $photoMapper->save();
                }
                if (!empty($photoData['cropFileNew']) && !empty($photoData['cropSizeNew'])) {
                    $photoData['cropFile'] = $photoData['cropFileNew'];
                    $photoData['cropSize'] = $photoData['cropSizeNew'];
                    unset($photoData['cropFileNew']);
                    unset($photoData['cropSizeNew']);
                }
            }

            if (!empty($photoData['cropFile']) && !empty($photoMapper->fileLocation)) {
                $sourcePath = $this->f3->get('BASEDIR') . '/' . $this->f3->get('UPLOADS') . 'crop-' . $photoMapper->id . '-' . time() . '.png';
                \Wyolution\Logger::debug('Storing: ' . $sourcePath);
                $imgBinary = base64_decode(str_replace('data:image/png;base64,','',$photoData['cropFile']));
                if ($imgBinary !== false) {
                    // delete existing one if only updating existin crop
                    $this->files->deleteFile($photoMapper->fileCropLocation??'');
                    
		            $fp = fopen($sourcePath, 'w');
		            fwrite($fp, $imgBinary);
		            fclose($fp);
		            $image = imagecreatefrompng($sourcePath);
	                imagejpeg($image, str_replace('.png','.jpg',$sourcePath),100);
	                imagedestroy($image);
	                unlink($sourcePath);
	                $sourcePath = str_replace('.png','.jpg',$sourcePath);
		            $uploadedPhotoPath = $this->files->storePhoto($photoMapper->id,$sourcePath);
		            if (!empty($uploadedPhotoPath)) {
                        $photoMapper->cropSize = empty($photoData['cropSize'])?NULL:$photoData['cropSize'];
                        $photoMapper->fileCropLocation = $uploadedPhotoPath;
                        $photoMapper->save();
		            }
                }
                else {
                    \Wyolution\Logger::error('Photo Crop(' . $photoMapper->id . ') failed to decode.');
                }
            }
        }
        
        return $photoMapper->cast();
    }
    
    public function deletePhoto(int $photoId):bool {
        if ($photoId < 1) {
            return false;
        }
        
        $photoMapper = new \DB\SQL\Mapper($this->db,'photos');
        $photoMapper->load(['id = ?',$photoId]);
		if ($photoMapper->dry()) {
			return false;
		}
		
		// delete guesses
		$guessMapper = new \DB\SQL\Mapper($this->db,'guesses');
		$guessList = $guessMapper->find(['photoId = ?',$photoId]);
		foreach ($guessList as $guess) {
		    $this->files->deleteFile($guess->fileLocation??'');
		}
		$guessMapper->erase(['photoId = ?',$photoId]);
		
		// delete files
		$this->files->deleteFile($photoMapper->fileLocation??'');
		$this->files->deleteFile($photoMapper->fileCropLocation??'');
		
		$photoMapper->erase();
		
		return true;
    }
    
    private function updatePhotoField(int $photoId, string $field, $fieldValue):bool {
        $photoData = $this->getPhoto($photoId);
        if (empty($photoData)) {
            return false;
        }
        
        $photoData[$field] = $fieldValue;
	    return $this->updatePhoto($photoData);
    }
    
    public function changePhotoStatus(int $photoId, int $photoStatus):bool {
        
        $photoStatus = intval($photoStatus);
        
        if (empty($photoId) || !in_array($photoStatus,[0,1,2])) {
            return false;
        }

	    return $this->updatePhotoField($photoId, 'active', $photoStatus);
    }
    
    public function updateGuess(int $photoId, int $guessId):bool {
        
        if (empty($photoId) || empty($guessId)) {
            return false;
        }
        
	    return $this->updatePhotoField($photoId, 'guessId', $guessId);
    }

}