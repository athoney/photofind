-- Adminer 4.7.4 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `auditlogs`;
CREATE TABLE `auditlogs` (
  `auditId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `eventTime` timestamp NOT NULL DEFAULT current_timestamp(),
  `ip` varchar(45) NOT NULL DEFAULT '',
  `info` text NOT NULL,
  PRIMARY KEY (`auditId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `auditlogs` (`auditId`, `username`, `eventTime`, `ip`, `info`) VALUES
(1,	'test@thoney.net',	'2019-10-27 22:29:05',	'127.0.0.1',	'Successful login, user = {\"userName\":\"test@thoney.net\",\"name\":\"Test Admin\",\"accessLevel\":\"ADMINISTRATOR\",\"accessLevelDesc\":\"ADMINISTRATOR\",\"accessLevelLabel\":\"Administrator\",\"userId\":1,\"defaultAccess\":{\"accessLevel\":\"ADMINISTRATOR\",\"accessLevelDesc\":\"ADMINISTRATOR\",\"accessLevelLabel\":\"Administrator\"},\"rememberMe\":\"off\"}');

DROP TABLE IF EXISTS `auth_tokens`;
CREATE TABLE `auth_tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `selector` varchar(64) NOT NULL,
  `validator` varchar(256) NOT NULL,
  `expires` datetime NOT NULL,
  `tokenType` enum('LOGIN_COOKIE','ACTIVATE','PASSWORD_RESET') NOT NULL DEFAULT 'LOGIN_COOKIE',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_tokens_userId_index` (`userId`),
  UNIQUE KEY `auth_tokens_selector_index` (`selector`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `guesses`;
CREATE TABLE `guesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(128) NOT NULL,
  `fileLocation` varchar(2000) NOT NULL,
  `submitDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `photoId` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `photos`;
CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateUploaded` timestamp NOT NULL DEFAULT current_timestamp(),
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `fileLocation` varchar(2000) DEFAULT NULL,
  `fileCropLocation` varchar(2000) DEFAULT NULL,
  `cropSize` int(11) DEFAULT NULL,
  `hint1` varchar(2000) DEFAULT NULL,
  `hint1Date` timestamp NULL DEFAULT NULL,
  `hint2` varchar(2000) DEFAULT NULL,
  `hint2Date` timestamp NULL DEFAULT NULL,
  `hint3` varchar(2000) DEFAULT NULL,
  `hint3Date` timestamp NULL DEFAULT NULL,
  `winnerId` int(11) DEFAULT NULL,
  `prizeId` int(11) DEFAULT NULL,
  `sponsorId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `photos` (`id`, `dateUploaded`, `name`, `description`, `active`, `fileLocation`, `fileCropLocation`, `cropSize`, `hint1`, `hint1Date`, `hint2`, `hint2Date`, `hint3`, `hint3Date`, `winnerId`, `prizeId`, `sponsorId`) VALUES
(1,	'2019-10-29 08:50:08',	'Sheridan College',	'computer lab',	1,	'/photos/p1/20191029025008-20181213064346-entrance-sheridan-college-wyoming.jpg',	'/photos/p1/crop-1-1572317408.jpg',	744,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	1,	1);

DROP TABLE IF EXISTS `prizes`;
CREATE TABLE `prizes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(2000) DEFAULT NULL,
  `photoId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `prizes` (`id`, `name`, `description`, `image`, `photoId`) VALUES
(1,	'Money',	'money',	'/prize/p1/20191029025110-20190208083325-moneyPiggyBank.jpg',	NULL);

DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE `sponsors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `logo` varchar(2000) DEFAULT NULL,
  `pocName` varchar(255) DEFAULT NULL,
  `pocPhone` varchar(255) DEFAULT NULL,
  `pocEmail` varchar(255) DEFAULT NULL,
  `dateUploaded` datetime NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT 0,
  `notes` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sponsors` (`id`, `name`, `description`, `address`, `website`, `logo`, `pocName`, `pocPhone`, `pocEmail`, `dateUploaded`, `active`, `notes`) VALUES
(1,	'Lululemon',	'mediocrity to greatness',	'123 Easy Street',	'https://shop.lululemon.com/',	'/sponsors/s1/20191029024353-download.png',	'Alicia',	'123-456-7890',	'alicia.lululemon@gmail.com',	'0000-00-00 00:00:00',	1,	NULL);

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(96) NOT NULL,
  `password` varchar(255) NOT NULL,
  `accessLevel` varchar(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT 1,
  `attempts` int(10) unsigned NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `name`, `username`, `password`, `accessLevel`, `active`, `attempts`) VALUES
(1,	'Test Admin',	'test@thoney.net',	'e77989ed21758e78331b20e477fc5582',	'ADMINISTRATOR',	1,	0),
(3,	'Alicia',	'alicia.thoney@gmail.com',	'e77989ed21758e78331b20e477fc5582',	'ADMINISTRATOR',	1,	0);

-- 2019-10-29 02:55:19
