<?php

namespace PhotoFind;

class Quest Extends ModelBase {
    
    /**
     * initialize class
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    static public function getQuestIdByPhotoId(int $photoId):int {
        $f3 = \Base::instance();
        $db = $f3->get("DB");
        $row = $db->exec('select questId from photo where Id = ?',$photoId);
        return $row[0]['questId']??0;
    }

    private function getTable(string $name):\DB\SQL\Mapper {
        $mapper = new \DB\SQL\Mapper($this->db,$name);
        return $mapper;
    }
    
    public function hasAccessToQuest(int $questId):bool {
        if (empty($questId)) {
            return false;
        }
        
        if ($this->permissions->isAdmin()) {
            return true;
        }

        $mapper = $this->getTable('quest');
        if ($mapper->count(['Id = ? and userId = ?',$questId,$this->permissions->getUserId()]) == 1) {
            return true;
        }
        
        return false;
    }
    /**
     * Setup an empty quest.
     * @param int $companyId
     * @return array
     */
    public function getEmptyQuest():array {
        $mapper = $this->getTable('quest');
        $fields = $mapper->fields();
        $emptyQuest = [];
        foreach ($fields as $field) {
            $emptyQuest[$field] = '';
        }
        return $emptyQuest;
    }
    
    public function getQuests():array {
        $mapper = $this->getTable('quest');
        if ($this->permissions->isAdmin()) {
            $mapper->load([],['order' => 'dateCreated desc']);
        }
        else {
            $mapper->load(['userId = ?',$this->permissions->getUserId()],['order' => 'dateCreated desc']);
        }
        return self::convertToHash($mapper);
    }
    
    public function getLiveQuests():array {
        $mapper = $this->getTable('quest');
        if ($this->permissions->isAdmin()) {
            $mapper->load(['status = ? or status = ?','live','preview'],['order' => 'dateCreated desc']);
        }
        else if ($this->permissions->isAccountManager()) {
            $mapper->load(['status = ? or (status = ? and userId = ?)','live','preview',$this->permissions->getUserId()],['order' => 'dateCreated desc']);
        }
        else {
            $mapper->load(['status = ?','live'],['order' => 'dateCreated desc']);
        }
        $questData = self::convertToHash($mapper); 
        // get photos
        $photo = new \PhotoFind\Photo();
        $user = new \PhotoFind\User();
        for ($i = 0; $i < count($questData); $i++) {
           $questData[$i]['photos'] = $photo->getQuestPhotos($questData[$i]['Id']);
           $questData[$i]['userData'] = $user->get($questData[$i]['userId']);
        }
        return $questData;
    }

    public function getLiveQuestsByAccount(int $userId):array {
        $mapper = $this->getTable('quest');
        $mapper->load(['status = ? and userId = ?','live',$userId],['order' => 'dateCreated desc']);
        $questData = self::convertToHash($mapper); 
        // get photos
        $photo = new \PhotoFind\Photo();
        $user = new \PhotoFind\User();
        for ($i = 0; $i < count($questData); $i++) {
           $questData[$i]['photos'] = $photo->getQuestPhotos($questData[$i]['Id']);
           $questData[$i]['userData'] = $user->get($questData[$i]['userId']);
        }
        return $questData;
    }
   
    public function getClosedQuest(int $questId):array {
        if ($questId == 0) {
            return [];
        }
        
        $mapper = $this->getTable('quest');
        $mapper->load(['Id = ? and status = ?',$questId,'closed']);
        
        if ($mapper->dry()) {
            return [];
        }

        $questData = $mapper->cast();
        $otherQuests = $this->getLiveQuestsByAccount($questData['userId']);
        $questData['otherQuests'] = $otherQuests;
        return $questData;
    }
    
    public function get(int $questId, $live = false, $forWin = false):array {
        if ($questId == 0) {
            return [];
        }
        
        $mapper = $this->getTable('quest');
        if ($live) {
            if ($this->permissions->isAdmin()) {
                $mapper->load(['Id = ? and (status = ? or status = ?)',$questId,'live','preview']);
            }
            else if ($this->permissions->isAccountManager()) {
                $mapper->load(['Id = ? and (status = ? or (status = ? and userId = ?))',$questId,'live','preview',$this->permissions->getUserId()],['order' => 'dateCreated desc']);
            }
            else {
                $mapper->load(['Id = ? and status = ?',$questId,'live']);
            }
        }
        else {
            if ($this->permissions->isAdmin()) {
                $mapper->load(['Id = ?',$questId]);
            }
            else if ($forWin) {
                $mapper->load(['Id = ?',$questId]);
            }
            else {
                $mapper->load(['Id = ? and userId = ?',$questId, $this->permissions->getUserId()]);
            }
        }
        
        $questData = self::convertToHash($mapper);
        
        if (count($questData) == 1) {
            $questData = $questData[0];
        }
        else {
            \Wyolution\Logger::error("Quest data for $questId not found or had more than one record.");
            return [];
        }
        
        // load photos
        $photo = new \PhotoFind\Photo();
        $questData['photos'] = $photo->getQuestPhotos($questData['Id']);
        
        // load guesses
        $rows = $this->db->exec('select photoId, count(*) as newGuesses from guesses where questId = ? and `match` = 1 group by photoId',$questId);
        $totalNewGuesses = 0;
        foreach ($rows as $photo) {
            $totalNewGuesses += $photo['newGuesses'];
            $questData['guesses'][$photo['photoId']] = $photo['newGuesses'];
        }
        $questData['guesses']['totalNewGuesses'] = $totalNewGuesses;

        return $questData;
    }
    
    public function delete(int $questId):bool {
        $mapper = $this->getTable('quest');
        if ($this->permissions->isAdmin()) {
            $mapper->load(['Id = ?',$questId]);
        }
        else {
            $mapper->load(['Id = ? and userId = ?',$questId, $this->permissions->getUserId()]);
        }
        if ($mapper->dry()) {
            return false;
        }
        
        $photo = new \PhotoFind\Photo();
        $photo->deleteAllPhotosByQuestId($questId);
            
        $mapper->erase(['Id = ?',$questId]);
        
        $winners = $this->getTable('winner');
        $winners->erase(['questId = ?',$questId]);

        return true;
    }
    
    public function addQuestPhoto(array $questData):bool {
        $photo = new \PhotoFind\Photo();
        if (!empty($questData['questPhoto'])) {
            $photo->storeQuestPhoto(intval($questData['photoId']),$this->permissions->getUserId(), $questData['Id'], $questData['questPhoto'], $questData['questPhotoCrop']);
        }
        return true;
    }

    public function deleteQuestPhoto(array $questData):bool {
        $photo = new \PhotoFind\Photo();
        if (!empty($questData['photoId'])) {
            return $photo->deletePhoto(intval($questData['photoId']));
        }
        else {
            return false;
        }
    }
    
    /**
     * Insert and update quest records.
     *
     * $questData['name'] is required.
     *
     * @param array $questData
     * @return array
     */
    public function update(array $questData):array {
        $mapper = $this->getTable('quest');
        
        if (empty($questData['Id'])) {
            // insert quest record
        }
        else {
            if ($this->permissions->isAdmin()) {
                $mapper->load(['Id = ?',$questData['Id']]);
            }
            else {
                $mapper->load(['Id = ? && userId = ?',$questData['Id'],$this->permissions->getUserId()]);
            }
            if ($mapper->dry()) {
                return [];
            }
        }
        
        $mapper->name                   = $questData['name'];
        $mapper->description            = $questData['description'];
        $mapper->socialMediaDescription = $questData['socialMediaDescription'];
        $mapper->prizeDesc              = $questData['prizeDesc'];
        $mapper->status                 = $questData['status'];
        if (empty($mapper->userId)) {
            $mapper->userId             = $this->permissions->getUserId();
        }
        $mapper->save();
        
        \Wyolution\Logger::debug('Save uploaded photos.');
        
        // Save photos
        if (!empty($questData['prizePhoto'])) {
            $photo = new \PhotoFind\Photo();
            $photo->storePrizePhoto(intval($questData['photoId']),$this->permissions->getUserId(), $mapper->Id, $questData['prizePhoto']);
        }
       
        return $mapper->cast();
    }
    
    public function getLeaderboard(int $questId, int $questPhotoCount):array {
        $sql = 'select photoId, email, `match` from guesses where questId = ? group by photoId, email, `match` order by `match` desc, submitDate asc';
        $sql = 'select * from guesses where questId = ? and `match` <> 1 order by submitDate, `match` desc';
        $rows = $this->db->exec($sql,$questId);
        $lb = []; //leaderboard
        foreach ($rows as $row) {
            $email = $row['email']; 
            if (empty($lb[$email])) {
                $lb[$email] = ['photos' => [], 'matchCount' => 0, 'completionDate' => '', 'info' => $row];
            }

            if (empty($lb[$email]['photos'][$row['photoId']]) || $lb[$email]['photos'][$row['photoId']]['match'] < $row['match']) {
                $lb[$email]['photos'][$row['photoId']]['match'] = $row['match'];
                $lb[$email]['photos'][$row['photoId']]['submitDate'] = $row['submitDate'];
                if ($row['match'] == 2 && (empty($lb[$email]['completionDate']) || $row['submitDate'] > $lb[$email]['completionDate'])) {
                    $lb[$email]['completionDate'] = $row['submitDate'];
                }
                if ($row['match'] == 2) {
                    $lb[$email]['matchCount']++;
                }
            }
        }
        
        $tlb     = []; // temp leader board, resort board
        $noMatch = [];
        for ($i = $questPhotoCount; $i > 0; $i--) {
            foreach ($lb as $email => $info) {
                if ($i == 1 && empty($info['completionDate'])) {
                    $noMatch[] = $email;
                }
                else if ($i == $info['matchCount']) {
                    $tlb[$i][$info['completionDate']] = $email;
                }
            }
        }
        
        //echo print_r($tlb, true); exit(0);
        
        // now that we have everyone grouped by how many photos they
        // they have matched, we can sort them based on the old submitted
        // date, thus giving us our leaderbard.
        $matchCounts = array_keys($tlb);
        foreach ($matchCounts as $matchCount) {
            ksort($tlb[$matchCount]);
        }
        
        // pull info on people already idenfied as winners
        $winnerMapper = new \PhotoFind\Winner();
        $winnerList = $winnerMapper->getWinnersByQuest($questId);

        // combine back everyone, including those that haven't found
        // a match.
        $flb = [];
        foreach ($tlb as $matchList) {
            foreach ($matchList as $email) {
                $flb[$email] = $lb[$email];
                if (!empty($winnerList[$email])) {
                    $flb[$email]['winnerInfo'] = $winnerList[$email];
                }
            }
        }
        foreach ($noMatch as $email) {
            $flb[$email] = $lb[$email];
        }
       
        return $flb; 
    }
    
}