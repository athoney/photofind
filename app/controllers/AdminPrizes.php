<?php

namespace controllers;

/**
 * 
 * Controller for sponsor administrative functions: 
 * - user CRUD
 * 
 * @package		controllers
 * 
 */
class AdminPrizes extends ControllerSecure {

	protected $f3;
	protected $view;
	protected $db;
	protected $isAdmin = false;
	protected $prizes;

	/**
	 * initialize controller
	 *
	 * @return void
	 */
	public function __construct() {
	    parent::__construct();
		$this->isAdmin = $this->permissions->hasManagerAccess();
		$this->prizes = new \PhotoFind\Prizes();
		//problem here
	}
	
	private function deflectNonAdmin() {
		if (!$this->isAdmin) {
			$this->f3->reroute('/');
		}
	}
	
	public function getPrizes() {
	    $this->deflectNonAdmin();
	   
	    $prizeList = $this->prizes->getPrizes();
	    
 	    $this->f3->set('_prizeList', $prizeList);
	    //$this->f3->set('_prizeList',$this->prizes->getPrizes);
	    $this->view->render('admin/prizes.html','admin/layout.secure.html');
	}
	
	public function getPrize() {
		$this->deflectNonAdmin();
		$prizeId = \Wyolution\F3Helpers::getParam('id',0);
		$this->f3->set('_prize',$this->prizes->getPrize($prizeId));
		$photos = new \PhotoFind\Photos();
		$this->f3->set('_photos',$photos->getPhotos(1));
		$this->view->render('admin/prize.html','admin/layout.secure.html');
	}
	
	public function update() {
		$prizeData = \Wyolution\F3Helpers::getParam('prize',[]);
		
		$prizeData = $this->prizes->updatePrize($prizeData);

		if (empty($prizeData['id'])) {
			$this->view->messageFailure('Prize Not Updated');
			$this->f3->reroute('/admin/prize');
		}
		else {
			$this->view->messageSuccess('Prize Updated Successfully');
			$this->f3->reroute('/admin/prize/' . $prizeData['id']);
		}
	}
/*
	public function changeStatus() {
		$sponsorData = \Wyolution\F3Helpers::getParam('sponsor', []);
		if (empty($sponsorData['id'])) {
			$this->view->messageFailure('No sponsor specified.');
		}
		else {		
			$result = $this->sponsors->changeStatus($sponsorData);
			if ($result) {
				$this->view->messageFailure('Unable to update sponsor status.');
			}
			else {
				$this->view->messageFailure('Sponsor status updated.');
			}
		}
		$this->f3->reroute('/admin/sponsors');
	}
*/
}
