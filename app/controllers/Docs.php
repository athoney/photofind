<?php

namespace controllers;


class Docs extends ControllerSecure {
	
	public function releaseNotes() {
		$this->view->render('admin/release.notes.html','admin/layout.secure.html');
	}
	
}