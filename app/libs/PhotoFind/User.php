<?php

namespace PhotoFind;

class User Extends ModelBase {
    
    /**
     * initialize class
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    
    private function getTable(string $name):\DB\SQL\Mapper {
        $mapper = new \DB\SQL\Mapper($this->db,$name);
        return $mapper;
    }
    
    /**
     * Setup an empty user.
     * @param int $companyId
     * @return array
     */
    public function getEmptyUser():array {
        $mapper = $this->getTable('user');
        $fields = $mapper->fields();
        $emptyUser = [];
        foreach ($fields as $field) {
            $emptyUser[$field] = '';
        }
        return $emptyUser;
    }
    
    public function getUsers():array {
        $mapper = $this->getTable('user');
        $mapper->load([],['order' => 'name']);
        return self::convertToHash($mapper);
    }
    
    public function get(int $userId):array {
        if ($userId == 0) {
            return [];
        }
        
        $mapper = $this->getTable('user');
        $mapper->load(['Id = ?',$userId]);
        
        return $mapper->cast();
    }
    
    /**
     * Insert and update user records.
     *
     * $userData['name'] is required.
     *
     * @param array $userData
     * @return array
     */
    public function update(array $userData):array {
        $mapper = $this->getTable('user');
        
        if (empty($userData['Id'])) {
            // insert user record
        }
        else {
            $mapper->load(['Id = ?',$userData['Id']]);
            if ($mapper->dry()) {
                return [];
            }
        }
        
        $file = new \PhotoFind\Files();
        $fileList = $file->getUploadedFile();
        
        $mapper->name          = $userData['name'];
        $mapper->username      = $userData['email'];
        $mapper->accessLevel   = $userData['accessLevel'];
        if (array_key_exists('password',$userData)) {
            $mapper->password  = md5($userData['password']);
        }
        $mapper->address       = $userData['address'];
        $mapper->address2      = $userData['address2'];
        $mapper->city          = $userData['city'];
        $mapper->state         = $userData['state'];
        $mapper->postalCode    = $userData['postalCode'];
        $mapper->website       = $userData['website'];
        $mapper->companyName   = $userData['companyName'];
        $mapper->companyEmail  = $userData['companyEmail'];
        $mapper->companyPhone  = $userData['companyPhone'];
        if (!empty($userData['deletePhoto'])) {
            $file->deleteFile($mapper->companyLogo);
            $mapper->companyLogo = '';
        }
        $mapper->active        = (intval($userData['active']) == 1)?1:0;
        $mapper->save();
        
        \Wyolution\Logger::debug("Upload logo:\n" . print_r($fileList,true));
        
        if (!empty($fileList) && strpos($fileList[0],'companyLogo')) {
            $sourceFile = $this->f3->get('BASEDIR') . '/' . $fileList[0];
            $targetPath = '/accounts/u' . $mapper->Id . '/logo.' . pathinfo($fileList[0],PATHINFO_EXTENSION);
            \Wyolution\Logger::debug('A');
            if ($file->storeFile($sourceFile,$targetPath)) {
                if (!empty($mapper->companyLogo) && $mapper->companyLogo != $targetPath) {
                    // delete old file
                    $file->deleteFile($mapper->companyLogo);
                }
                \Wyolution\Logger::debug('B');
                $file->resizePhoto($targetPath, '800x600'); // resize image
                $mapper->companyLogo = $targetPath;
                $mapper->save();
            }
            \Wyolution\Logger::debug('C');
        }
        
        return $mapper->cast();
    }
    
}