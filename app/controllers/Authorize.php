<?php

namespace controllers;

/**
 * Handle user authentication tasks:
 * login (inc. remembered login), logout, reset password
 *
 * @package		controllers
 */
class Authorize extends ControllerPublic {

	const LOGIN_ERROR_MSG = 'Credentials supplied are not valid.  Check to make sure your email and password are correct.';
	const LOCKOUT_ERROR_MSG = 'This account is locked.  Please contact system administrator to resolve.';
	const EXPIRED_WARN_MSG = 'Your remembered credentials have expired. Please log in again.';
	
	public function login() {
	    if (!empty($this->f3->get('SESSION.userIsLoggedIn')) && 
	           !empty($this->f3->get('SESSION.user')) &&
	           $this->f3->get('SESSION.userIsLoggedIn') == '1') {
	       $this->f3->reroute('/');
        }
        else {
            $authTokenMgr = new \PhotoFind\AuthTokenMgr();
            $cookie = $authTokenMgr->getCookie();
            if (!empty($cookie)) {
                $this->validate();
            }
            else {
                $this->view->render('admin/login.html','admin/layout.html');
            }
        }
	}


	/**
	 * log user in
	 *
	 * @return void
	 */
	public function validate() {
		\Wyolution\Logger::debug('Authorize::login');
		$loginSucceeded = false;
		$destinationRoute = $this->f3->get('SESSION.destinationRoute');

		$authTokenMgr = new \PhotoFind\AuthTokenMgr();

		// User can log in with either a username/password or 
		// with a rememberMe cookie.
		// But if the user presents with a username and password,
		// it makes sense to validate them rather than the cookie value.
		$msg = '';
		$username = \Wyolution\F3Helpers::getParam('username', '');
		$password = \Wyolution\F3Helpers::getParam('password', '');

		if (!empty($username) &&  !empty($password)) {
			$db = $this->f3->get('DB');
			$userMapper = new \DB\SQL\Mapper($db, 'user',null,0);
			$userMapper->load(array('username=? and attempts < 5', $username));
			if (!$userMapper->dry()) {
				if ($userMapper->attempts > 4 || $userMapper->active == 0 ||$userMapper->password != md5($password)) {
					$userMapper->attempts++;
					if ($userMapper->attempts > 4 || $userMapper->active == 0) {
						$userMapper->active = 0;
						$msg = self::LOCKOUT_ERROR_MSG;
						\Wyolution\Audit::log('Failed login, bad password, account locked.', $username);
					} else {
						\Wyolution\Audit::log('Failed login, bad password.', $username);
					}
					$userMapper->save();
				} else if (empty(\PhotoFind\Permissions::USER_ROLES[$userMapper->accessLevel])) {
					\Wyolution\Audit::log('Failed login, undefined role.');
				} else {
					$loginSucceeded = true;
					$user = [
					        'username' => $userMapper->username,
					        'name' => $userMapper->name,
					        'accessLevel' => $userMapper->accessLevel,
					        'accessLevelLabel' => \PhotoFind\Permissions::USER_ROLES[$userMapper->accessLevel],
					        'Id' => $userMapper->Id
					];
					
					$userMapper->attempts = 0;
					$userMapper->save();

					$rememberMe = \Wyolution\F3Helpers::getParam('rememberMe', '');
					if ($rememberMe == '1') {
						$user['rememberMe'] = 'on';
						$authTokenMgr->generateAndStoreNewAuthToken($userMapper->Id);
					} else {
						$user['rememberMe'] = 'off';
					}
				}
			} else {
				\Wyolution\Audit::log('Failed login.', $username);
			}
		} else {
			// username and password are both blank, let's see if we have a cookie
			$cookie = $authTokenMgr->getCookie();
			if (!empty($cookie)) {
				\Wyolution\Logger::debug("have rememberMe cookie: $cookie");
				$user = $authTokenMgr->getUserFromAuthToken($cookie);
				if ($user !== null) {
					$loginSucceeded = true;
					$user['rememberedLogin'] = true;
				} 
			}
		}

		if ($loginSucceeded) {
			\Wyolution\Logger::debug("login succeeded, will store user info in session");
			$this->f3->set('SESSION.userIsLoggedIn', '1');
			$this->f3->set('SESSION.user', $user);
			$info = json_encode($user);
			// MTMTMT -- is this too much info to log about a user?, password is not included
			\Wyolution\Audit::log("Successful login, user = $info", $user['userName']);
			
			// Was the user on his way somewhere specific when we made him log in
			if (!empty($destinationRoute)) {
			    $this->f3->set('SESSION.destinationRoute', '');
			    $this->f3->reroute($destinationRoute);
			} else {
			    if ($this->permissions->isUser()) {
				    $this->f3->reroute('/');
			    }
			    else {
				    $this->f3->reroute('/dashboard');
			    }
			}
		} else {
			\Wyolution\Logger::debug("login failed, will clear user info from session");
			$this->f3->clear('SESSION.userIsLoggedIn');
			$this->f3->clear('SESSION.user');
			// Unsuccessful login attempt, go to login screen and give the user
			// some negative feedback
			if ($msg == '' && ($username != '' || $password != '')) {
				$msg = self::LOGIN_ERROR_MSG;
			}
			if ($msg != '') {
				$this->view->messageError($msg);
			}
			$this->view->render('admin/login.html','admin/layout.html');
		}

	}

	/**
	 * send the user an email with a token to allow her to reset her password
	 *
	 * @return void
	 */
	public function requestReset() {
		\Wyolution\Logger::debug('Authorize::requestReset');
		$this->view->messageError('Password reset feature not yet implemented. Sorry.');
		$this->view->render('login.html');
	}

	/**
	 * log user out
	 *
	 * @return void
	 */
	public function logout() {
		\Wyolution\Logger::debug('Authorize::logout');

		// AG to figure out: SESSION is already cleaned out by the time we get here
		// So despite what the code says it is doing, we can't get the username to log. Which is no biggie.
		// But if SESSION is already clear by the time this runs, then clearly
		// I'm not understanding something.
		$f3 = \Base::instance();
		$userName = $f3->get('SESSION.user.userName');

		// Do we have a rememberMe cookie for the current session?
		$authTokenMgr = new \PhotoFind\AuthTokenMgr();
		$cookie = $authTokenMgr->getCookie();
		if (!empty($cookie)) {
			$authTokenMgr->forgetAuthToken($cookie);
		}

		$f3->clear('SESSION');
		\Wyolution\Audit::log("logout user $userName");

		$this->view->render('admin/login.html','admin/layout.html');
	}
}