<?php

namespace controllers;

/**
 * 
 * Controller for sponsor administrative functions: 
 * - user CRUD
 * 
 * @package		controllers
 * 
 */
class AdminGuesses extends ControllerSecure {

	protected $f3;
	protected $view;
	protected $db;
	protected $isAdmin = false;
	protected $guesses;
	protected $photos;

	/**
	 * initialize controller
	 *
	 * @return void
	 */
	public function __construct() {
	    parent::__construct();
		$this->isAdmin = $this->permissions->hasAccountManagerAccess();
		$this->guesses = new \PhotoFind\Guesses();
		$this->photos = new \PhotoFind\Photos();
		
		//problem here
	}
	
	private function deflectNonAdmin() {
		if (!$this->isAdmin) {
			$this->f3->reroute('/');
		}
	}
	
	public function getGuesses() {
	    $this->deflectNonAdmin();
	    $photoId = \Wyolution\F3Helpers::getParam('id',0);
	    $guessList = $this->guesses->getGuesses($photoId);
	    
 	    $this->f3->set('_guessList', $guessList);
 	    $this->f3->set('_photo', $this->photos->getPhoto($photoId));
	    //$this->f3->set('_prizeList',$this->prizes->getPrizes);
	    $this->view->render('admin/guesses.1.html','admin/layout.secure.html');
	}
	
	public function getQuests(){
	    $this->view->render('admin/guesses.html','admin/layout.secure.html');
	}
	
	public function getQuestPhotos(){
	    $this->view->render('admin/questPhotos.html','admin/layout.secure.html');
	}
	
	public function getPhotoGuesses(){
	    $this->view->render('admin/photoGuesses.html','admin/layout.secure.html');
	}
	/*
		$this->f3->set('SESSION.dashboard.activeSponsors',$active);
	    
		$this->f3->set('_sponsorList', []);
		$this->f3->set('_sponsorList',$this->sponsors->getSponsors($active));
		$this->view->render('admin/sponsors.html','admin/layout.secure.html');
	*/
	
	public function getPhotos() {
	    $this->deflectNonAdmin();
	    
	    $active = \Wyolution\F3Helpers::getParam('active','');
	    if ($active == '1') {
	        $this->f3->set('SESSION.dashboard.activePhotos',1);
	    }
	    elseif ($active == '0') {
	        $this->f3->set('SESSION.dashboard.activePhotos',0);
	    }
	    $active = $this->f3->get('SESSION.dashboard.activePhotos');
	    
	    if ($active != '0') {
	        $active = 1;
	    }
	    else {
	        $active = 0;
	    }
	    $this->f3->set('SESSION.dashboard.activePhotos',$active);
	    
		$this->f3->set('_photoList', []);
		$this->f3->set('_photoList',$this->photos->getPhotos($active));
		$this->view->render('admin/guesses.html','admin/layout.secure.html');
	}
	
	public function getGuess() {
		$this->deflectNonAdmin();
		$guessId = \Wyolution\F3Helpers::getParam('id',0);
		$this->f3->set('_guess',$this->guesses->getGuess($guessId));
		$this->view->render('admin/guess.html','admin/layout.secure.html');
	}
	
	public function update() {
		$sponsorData = \Wyolution\F3Helpers::getParam('sponsor',[]);
		
		$sponsorData = $this->sponsors->updateSponsor($sponsorData);

		if (empty($sponsorData['id'])) {
			$this->view->messageFailure('Sponsor Not Updated');
			$this->f3->reroute('/admin/sponsor');
		}
		else {
			$this->view->messageSuccess('Sponsor Updated Successfully');
			$this->f3->reroute('/admin/sponsor/' . $sponsorData['id']);
		}
	}

	public function changeStatus() {
		$sponsorData = \Wyolution\F3Helpers::getParam('sponsor', []);
		if (empty($sponsorData['id'])) {
			$this->view->messageFailure('No sponsor specified.');
		}
		else {		
			$result = $this->sponsors->changeStatus($sponsorData);
			if ($result) {
				$this->view->messageFailure('Unable to update sponsor status.');
			}
			else {
				$this->view->messageFailure('Sponsor status updated.');
			}
		}
		$this->f3->reroute('/admin/sponsors');
	}
}
