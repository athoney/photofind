ALTER TABLE `guesses` 
ADD COLUMN `name` VARCHAR(50) NOT NULL AFTER `id`;

ALTER TABLE `guesses` 
ADD COLUMN `active` INT NOT NULL DEFAULT 0 AFTER `photoId`;

ALTER TABLE `guesses` 
CHANGE COLUMN `fileLocation` `fileLocation` VARCHAR(2000) NULL ;
