<?php

namespace PhotoFind;

class Winner extends ModelBase {
    protected $f3;
    protected $db;
    
    public function __construct()
    {
        $this->f3 = \Base::instance();
        $this->db = $this->f3->get("DB");
    }

    private function generateClaimCode(int $questId):string {
        $winnerMapper = new \DB\SQL\Mapper($this->db,'winner');
        $tokens = ['B', 'C', 'D', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'V', 'W', 'X', 'Y', 'Z', 'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z', '2', '3', '4', '5', '6', '7', '8', '9'];
        $max = count($tokens) - 1;
        $claimCode = $tokens[rand(0,$max)] . $tokens[rand(0,$max)] . $tokens[rand(0,$max)] . $tokens[rand(0,$max)] . $tokens[rand(0,$max)];
        while ($winnerMapper->count(['questId = ? and claimCode = ?',$questId,$claimCode]) > 0) {
            $claimCode = $tokens[rand(0,$max)] . $tokens[rand(0,$max)] . $tokens[rand(0,$max)] . $tokens[rand(0,$max)] . $tokens[rand(0,$max)];
        }
        return $claimCode;
    }

    public function addWinner(int $questId, string $email):bool {
        $winnerMapper = new \DB\SQL\Mapper($this->db,'winner');
		$winnerMapper->load(['questId = ? and email = ?',$questId,$email]);
		if ($winnerMapper->dry()) {
		    $winnerMapper->questId = $questId;
		    $winnerMapper->email   = $email;
		    $winnerMapper->claimCode = $this->generateClaimCode($questId);
		    $winnerMapper->active = 1;
		}
		else {
		    $winnerMapper->active = 1;
		}
		$winnerMapper->save();
		return true;
    }
    
     public function removeWinner(int $questId, string $email):bool {
        $winnerMapper = new \DB\SQL\Mapper($this->db,'winner');
		$winnerMapper->load(['questId = ? and email = ?',$questId,$email]);
		if (!$winnerMapper->dry()) {
		    $winnerMapper->active = 0;
		    $winnerMapper->save();
		}
		return true;
    }

    public function updatePrize(int $questId, string $email, int $prize):bool {
        $winnerMapper = new \DB\SQL\Mapper($this->db,'winner');
        $winnerMapper->load(['questId = ? and email = ?',$questId,$email]);
        if (!$winnerMapper->dry()) {
            $winnerMapper->prizeClaimed = $prize;
            $winnerMapper->prizeClaimedDate = date('Y-m-d H:i:s');
            $winnerMapper->save();
        }
        return true;
    }

    public function getWinnersByQuest(int $questId):array {
        $winnerMapper = new \DB\SQL\Mapper($this->db,'winner');
        $winnerMapper->load(['questId = ? and active = 1',$questId]);
        $winnerDetail = [];
        while (!$winnerMapper->dry()) {
            $winnerDetail[$winnerMapper->email] = $winnerMapper->cast();
            $winnerMapper->next();
        }
        return $winnerDetail;
    }
    
    public function getWinners():array {
        $winnerMapper = new \DB\SQL\Mapper($this->db,'winner');
        $winnerMapper->load(['active = 1'],['order' => 'prizeClaimedDate desc']);
        
        $quest = new \PhotoFind\Quest();
        $guesses = new \PhotoFind\Guesses();
        $user = new \PhotoFind\User();

        $winnerDetail = [];
        while (!$winnerMapper->dry()) {
            $winnerInfo = $winnerMapper->cast();
            $winnerInfo['questInfo'] = $quest->get($winnerInfo['questId'],false,true);
            if ($winnerInfo['questInfo']['status'] == 'closed') {
                $winnerInfo['displayName'] = $guesses->getDisplayName($winnerInfo['questId'],$winnerInfo['email']);
                $winnerInfo['userInfo'] = $user->get($winnerInfo['questInfo']['userId']);
                $winnerDetail[] = $winnerInfo;
            }
            $winnerMapper->next();
        }
        return $winnerDetail;
    }
   
}