<?php

namespace controllers;

/**
 *
 * @package controllers
 *        
 */
class Main extends ControllerPublic {
	
	/**
	 * Get assets needed to render homepage
	 *
	 * @return void
	 */
	public function home() {
	    $quest = new \PhotoFind\Quest();
	    $quests = $quest->getLiveQuests();
		$this->f3->set('_quests',$quests);
		$this->view->render('home.html','layout.html');
	}

	public function quest(){
	    $questId = \Wyolution\F3Helpers::getParam('questId', 0);
	    if (empty($questId)) {
	        $this->reroute('/');
	    }
	    $quest = new \PhotoFind\Quest();
	    $questData = $quest->get($questId,true);

	    if (empty($questData)) {
	        // check to see if quest is closed
    	    $questData = $quest->getClosedQuest($questId);
    	    if (empty($questData)) {
        	    $this->view->render('quest.html','layout.html');
        	    return;
    	    }
	    }

	    $user = new \PhotoFind\User();
	    $userData = $user->get($questData['userId']);
	    $questData['userData'] = $userData;
		$this->f3->set('_quest',$questData);
		$this->f3->set('_socialMediaShare',1);
	    $this->view->render('quest.html','layout.html');
	}

	public function login(){
	    $this->view->render('userLogin.html');
	}
	public function howToPlay(){
	    $this->view->render('howToPlay.html', 'layout.html');
	}
	public function tandc(){
	    $this->view->render('terms.and.conditions.html', 'layout.html');
	}
	public function privacy(){
	    $this->view->render('privacy.html', 'layout.html');
	}
	public function winners(){
	    $winner = new \PhotoFind\Winner();
	    $this->f3->set('_winners',$winner->getWinners());
	    $this->view->render('winners.html', 'layout.html');
	}
	//HERE
	public function photo() {
	    $id = \Wyolution\F3Helpers::getParam('photoId', 0);
	                                                                                          
	    $dataMapper = new \DB\SQL\Mapper($this->db,'photos');
	    $photo = $dataMapper->load(['id=?', $id]);
	    
	    $sponsorMapper = new \DB\SQL\Mapper($this->db,'sponsors');
	    $sponsorMapper->load(['id=?', $photo->sponsorId]);
	    
	    $prizeMapper = new \DB\SQL\Mapper($this->db,'prizes');
	    $prizeMapper->load(['id=?', $photo->prizeId]);
	    
	    $guessMapper = new \DB\SQL\Mapper($this->db,'guesses');
	    $guessCount = $guessMapper->count(['photoId=?', $id]);
	    
	    $this->f3->set('_guessCount', $guessCount);
		$this->f3->set('_photo', $photo->cast());                                  
		$this->f3->set('_sponsor', $sponsorMapper->cast());
		$this->f3->set('_prize', $prizeMapper->cast());
		$this->view->render('photo.html','layout.html');                                                                                                         
	}
	
	function helloWorld() {
	    $myDate = date('M jS, Y g:i:s a');
	    $this->f3->set('_myViewDate',$myDate);
	    $this->view->render('hello.html','layout.html');
	}
	
	function galleryPhotos() {
		$dataMapper = new \DB\SQL\Mapper($this->db,'photos');
        $data = $dataMapper->find(['active=2'],['order' => "dateUploaded DESC"]);
        $photos = [];
        foreach($data as $photo){
            $photos[] = $photo->cast();
        }
        
        $this->f3->set('_photos',$photos);
	    $this->view->render('gallery.html','layout.html');
	}
	
	function record(){
		$guessData = \Wyolution\F3Helpers::getParam('guess',[]);
		
		// get file
		if (!isset($guessData['questId'])) {
			$this->f3->reroute('/');
			return;
		}
		
		$files = new \PhotoFind\Files();
		$isPhotoPresent = $files->wasPhotoUploaded();
		
		/*
		if (empty($guessData['name']) || empty($guessData['username']) || empty($guessData['email']) || !$isPhotoPresent) {
			$this->view->messageError('Please provide all required data.');
			$this->f3->reroute('/photo/' . $guessData['photoId']);
			return;
		}
		*/

		$guess = new \PhotoFind\UserGuesses();
		$saved = $guess->recordGuess($guessData['questId'],$guessData['photoId'],$guessData['name'],$guessData['username'],$guessData['email'],$files);
		
		if (!$saved) {
			$this->view->messageError('Guess not recorded, please try again.');
		}
		else {
			$this->view->messageSuccess('Thank you, your guess has been saved.');
		}
		$this->f3->reroute('/quest/' . $guessData['questId']);
	}
	
}