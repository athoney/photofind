<?php

namespace controllers;

/**
 * 
 * Controller for photo administrative functions: 
 * - user CRUD
 * 
 * @package		controllers
 * 
 */
class AdminPhotos extends ControllerSecure {

	protected $f3;
	protected $view;
	protected $db;
	protected $isAdmin = false;
	protected $photos;

	/**
	 * initialize controller
	 *
	 * @return void
	 */
	public function __construct() {
	    parent::__construct();
		$this->isAdmin = $this->permissions->hasManagerAccess();
		$this->photos = new \PhotoFind\Photos();
	}
	
	private function deflectNonAdmin() {
		if (!$this->isAdmin) {
			$this->f3->reroute('/');
		}
	}
	
	public function getPhotos() {
	    $this->deflectNonAdmin();
	    
	    $active = \Wyolution\F3Helpers::getParam('active','');
	    if ($active == '1') {
	        $this->f3->set('SESSION.dashboard.activePhotos',1);
	    }
	    elseif ($active == '0') {
	        $this->f3->set('SESSION.dashboard.activePhotos',0);
	    }
	    $active = $this->f3->get('SESSION.dashboard.activePhotos');
	    
	    if ($active != '0') {
	        $active = 1;
	    }
	    else {
	        $active = 0;
	    }
	    $this->f3->set('SESSION.dashboard.activePhotos',$active);
	    
		$this->f3->set('_photoList', []);
		$this->f3->set('_photoList',$this->photos->getPhotos($active));
		
		$sponsors = new \PhotoFind\Sponsors();
		$this->f3->set('_sponsors',$sponsors->getSponsors(1));
		$prizes = new \PhotoFind\Prizes();
		$this->f3->set('_prizes',$prizes->getPrizes(1));
		
		$this->view->render('admin/photos.html','admin/layout.secure.html');
	}
	
	public function getPhoto() {
		$this->deflectNonAdmin();
		$photoId = \Wyolution\F3Helpers::getParam('id',0);
		$this->f3->set('_photo',$this->photos->getPhoto($photoId));
		$sponsors = new \PhotoFind\Sponsors();
		$this->f3->set('_sponsors',$sponsors->getSponsors(1));
		$prizes = new \PhotoFind\Prizes();
		$this->f3->set('_prizes',$prizes->getAvailablePrizes());
		$this->view->render('admin/photo.html','admin/layout.secure.html');

	}
	
	//that's it, really?//
	public function update() {
		$photoData = \Wyolution\F3Helpers::getParam('photo',[]);
		
		$photoData = $this->photos->updatePhoto($photoData);
		
		if (empty($photoData['id'])) {
			$this->view->messageFailure('Photo Not Updated');
			$this->f3->reroute('/admin/photo');
		}
		else {
			$this->view->messageSuccess('Photo Updated Successfully');
			$this->f3->reroute('/admin/photo/' . $photoData['id']);
		}
	}
	
	public function delete() {
		$this->deflectNonAdmin();
		$photoId = \Wyolution\F3Helpers::getParam('id',0);
		if ($this->photos->deletePhoto($photoId)) {
			$this->view->messageSuccess('Photo and all associated data deleted.');	
		}
		else {
			$this->view->messageFailure('Unable to delete photo.');
		}
		$this->f3->reroute('/admin/photos');
	}

	public function changeStatus() {
		$photoData = \Wyolution\F3Helpers::getParam('photo', []);
		if (empty($photoData['id'])) {
			$this->view->messageFailure('No photo specified.');
		}
		else {		
			$result = $this->photos->changeStatus($photoData);
			if ($result) {
				$this->view->messageFailure('Unable to update photo status.');
			}
			else {
				$this->view->messageFailure('Photo status updated.');
			}
		}
		$this->f3->reroute('/admin/photos');
	}
}
