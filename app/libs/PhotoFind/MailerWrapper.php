<?php


namespace PhotoFind;


class MailerWrapper extends \Wyolution\Mailgun {

	function __construct() {
		\Wyolution\Logger::debug("in MailerWrapper constructor");
		parent::__construct();
	}

	public function sendPasswordResetEmail($emailAddress, $authToken) {
		\Wyolution\Logger::debug("in sendPasswordResetEmail");
		
		$f3 = \Base::instance();

		$appName = $f3->get('_applicationName');
		$baseUrl = $f3->get('REALM');
		$baseUrl = str_replace('request', 'reset', $baseUrl);
		$link = $baseUrl . "?token=" . $authToken;

		$text = <<<EOT
Hi there!<br><br>
We just got a password reset request for your account at $appName.<br><br>
Click here to reset your password:<br>
$link<br><br>
Ignore this email if you didn't request it. <br><br>
Cheers,<br>
Will, your friendly $appName email robot 
EOT;
		
        $sentOk = $this->sendMail($emailAddress, "Reset your $appName password", $text);
		return $sentOk;
	}
	
	// This app does not support self-service registration, so we don't 
	// implement a wrapper for sendConfirmationEmail.
	// Refer to the SPG GraphOpus app if you need that functionality.

}
