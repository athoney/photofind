<?php

namespace PhotoFind;

class Guesses extends ModelBase {

    protected $f3;
    protected $db;
    protected $files;
    
    public function __construct()
    {
        $this->f3 = \Base::instance();
        $this->db = $this->f3->get("DB");
        $this->files = new \PhotoFind\Files();
    }
    
    public function getGuess(int $guessId):array {
        $guess = [];
        if (empty($guessId)) {
            return $guess;
        }
        $guessMapper = new \DB\SQL\Mapper($this->db,'guesses');
		$guessMapper->load(['Id = ?',$guessId]);
		if (!$guessMapper->dry()) {
		    $guess = $guessMapper->cast();
		}
		return $guess;
    }
    
    public function getPhotos(int $status):array {
		$photoMapper = new \DB\SQL\Mapper($this->db,'photos');
		$photoList = $photoMapper->find();
		$filteredPhotoList = [];
		foreach ($photoList as $photo) {
		    if ($status == 1 && $photo->active == 0) {
		        continue;
		    }
		    $filteredPhotoList[] = $photo->cast();
		}
        return $filteredPhotoList;
    }
    
    public function getGuesses(int $questId, int $photoId):array {
		$guessesMapper = new \DB\SQL\Mapper($this->db,'guesses');
		$guessesMapper->load(['photoId = ?',$photoId],['order' => 'match DESC, Id DESC']);
		
		$guessList = self::convertToHash($guessesMapper);
		
		$quest = new \PhotoFind\Quest();
		$questData = $quest->get($questId);
		if (empty($questData)) {
		    // may not have been authorized to view this quest
		    return [];
		}
	   
		$photoData = [];
		for ($i = 0; $i < count($questData['photos']['questphoto']); $i++) {
		    if ($questData['photos']['questphoto'][$i]['Id'] == $photoId) {
		        $photoData = $questData['photos']['questphoto'][$i];
		        break;
		    }
		}

		$questData['guessList'] = $guessList;
		$questData['photo'] = $photoData;
		return $questData;
    }
    
    public function deleteGuess(int $guessId): bool {
		$guessesMapper = new \DB\SQL\Mapper($this->db,'guesses');
		$guessesMapper->load(['Id = ?',$guessId]);
		if ($guessesMapper->dry()) {
		    return true;
		}
		
		$files = new \PhotoFind\Files();
		$files->deleteFile($guessesMapper->fileLocation);

		$guessesMapper->erase();
		return true;
    }
    
    public function updateMatch(int $guessId, int $match):bool {
		$guessesMapper = new \DB\SQL\Mapper($this->db,'guesses');
		$guessesMapper->load(['Id = ?',$guessId]);
		if (!$guessesMapper->dry()) {
		    $guessesMapper->match = $match;
		    $guessesMapper->save();
		    return true;
		}
		
		return false;
    }

    public function getDisplayName(int $questId, string $email):string {
        $guessMapper = new \DB\SQL\Mapper($this->db,'guesses');
		$guessMapper->load(['questId = ? and email = ? and `match` = 2',$questId,$email],['order' => '`match` desc, submitDate desc', 'limit' => 1]);
		if (!$guessMapper->dry()) {
		    return $guessMapper->username;
		}
		else {
		    return '';
		}
    }
   
}