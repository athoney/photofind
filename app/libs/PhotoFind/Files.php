<?php

namespace PhotoFind;

class Files
{
    protected $f3;
    protected $db;
    protected $basePath = '';
    protected $uploadedFiles = [];
    
    public function __construct()
    {
        $this->f3 = \Base::instance();
        $this->db = $this->f3->get("DB");
        
        $this->basePath = $this->f3->get('BASEDIR') . '/data';
    }
    
    /**
     * Get file uploaded via browser
     *
     */
    //private function getUploadedFile():array {
    public function getUploadedFile():array {
        
        if (!empty($this->uploadedFiles)) {
            return $this->uploadedFiles;
        }

	    $web = \Web::instance();
	    
	    $files = $web->receive(
            function($file,$formFieldName){
                // maybe you want to check the file size
                if($file['size'] > (15 * 1024 * 1024)) return false; // if bigger than 15 MB, this file is not valid, return false will skip moving it
                
                // can also check to make sure a photo was uploaded (.png, .gif, .jpg, etc.)
                
                // everything went fine, hurray!
                $ext = strtolower(pathinfo($file['name'], PATHINFO_EXTENSION));
                return in_array($ext,['png','jpg','jpeg','gif','webp','heic','heif']);
            },
            true,
            function($file, $formFieldName) {
                $filename = preg_replace("/[^A-Za-z0-9\_\-\.]/", '_', $file);
                $filename = preg_replace("/_+/", '_', $filename);
                return $formFieldName . '-pf-' . date('Ymdhis') . '-' . $filename;
            }
        );
        
        $uploadedFiles = [];
        foreach ($files as $file => $uploaded) {
            if ($uploaded == 1) {
                $uploadedFiles[] = $file;
            }
        }
        
        \Wyolution\Logger::debug("Found files:\n" . print_r($uploadedFiles,true));
        
        $this->uploadedFiles = $uploadedFiles;
        
        return $uploadedFiles;
    }
    
    protected function isPresent(string $filePath, bool $override = false):bool {
        $mediaLibraryBasePath = $this->basePath;
        if (!$override) {
            $filePath = $mediaLibraryBasePath . $filePath;
        }
        return file_exists($filePath);
    }
    
    /** 
     * Move a file from its source to the media library.  Check to make sure the path 
     * exists.  If it doesn't, create it.
     * 
     **/
    public function storeFile(string $fileSourcePath, string $fileTargetPath, bool $override = false):bool {
        $mediaLibraryBasePath = $this->basePath;
        // if override false, then use media library.  This would allow certain files to be written to
        // completely different locations if needed (like guesses)
        if (!$override) {
            $fileTargetPath = $mediaLibraryBasePath . $fileTargetPath;
        }
        $targetPath = dirname($fileTargetPath);
        
        if (!file_exists($targetPath)) {
            $oldmask = umask(0);
            \Wyolution\Logger::debug("Base Path: $mediaLibraryBasePath / Make: $targetPath");
            mkdir($targetPath, 0777, true);
            umask($oldmask);
        }
        \Wyolution\Logger::debug("Storing file:\n" . $fileSourcePath . ' => ' . $fileTargetPath);
        $fileCreationResult = rename($fileSourcePath, $fileTargetPath);
        if (!$fileCreationResult) {
            \Wyolution\Logger::error('Failed to store file: ' . $fileTargetPath);
        }        
        return $fileCreationResult;
    }
    
    public function deleteFile(string $fileTargetPath, bool $override = false) {
        if (empty($fileTargetPath)) {
            return;
        }
        $mediaLibraryBasePath = $this->basePath;
        if (!$override) {
            $fileTargetPath = $mediaLibraryBasePath . $fileTargetPath;
        }
        if (!empty($fileTargetPath) && file_exists($fileTargetPath) && !is_dir($fileTargetPath)) {
            unlink($fileTargetPath);
            // now try deleting folder (will fail on if not empty, but no big deal)
            //unlink(dirname($fileTargetPath));
        }
    }

    public function wasPhotoUploaded():bool {
        $uploadedPhoto = $this->getUploadedFile();
        return !empty($uploadedPhoto[0]);
    }
    
    public function storeGuess(int $photoId, int $guessId):string {
        $uploadedPhoto = $this->getUploadedFile();
        
        if (empty($uploadedPhoto[0])) {
            \Wyolution\Logger::debug('No uploaded guess photo found.');
            return false;
        }
        
        $uploadedPhotoPath = $uploadedPhoto[0];
        $targetPhotoPath = '/guesses/p' . $photoId . '/g' . $guessId . '/' .basename($uploadedPhotoPath);
        $result = $this->storeFile($uploadedPhotoPath,$targetPhotoPath);
        
        if (!$result) {
            return '';
        }

        $sourcePhotoPath = $targetPhotoPath;

        $ext = '.' . pathinfo($sourcePhotoPath, PATHINFO_EXTENSION);
        $targetPhotoPath = str_replace($ext,'.jpg',$sourcePhotoPath);
        $this->resizePhoto($sourcePhotoPath, "1024x768", $targetPhotoPath);

        if ($this->isPresent($targetPhotoPath)) {
            if ($sourcePhotoPath != $targetPhotoPath) {
                $this->deleteFile($sourcePhotoPath);
            }
            return $targetPhotoPath;
        }
        else if ($this->isPresent($sourcePhotoPath)) {
            return $sourcePhotoPath;
        }
        else {
            return '';
        }
    }
    
    /**
     * Resize a graphic file.
     *
     **/
    public function resizePhoto(string $fileSourcePath, string $size, string $fileTargetPath = '', bool $override = false):bool {
        $mediaLibraryBasePath = $this->basePath;
        // if override false, then use media library.  This would allow certain files to be written to
        // completely different locations if needed (like guesses)
        if (!$override) {
            $fileSourcePath = $mediaLibraryBasePath . $fileSourcePath;
        }
        
        if (!file_exists($fileSourcePath)) {
            \Wyolution\Logger::debug('Source not found: ' . $fileSourcePath);
            return false;
        }
        
        if (empty($fileTargetPath)) {
            // overwrite existing image with new sized image
            $fileTargetPath = $fileSourcePath;
        }
        else if (!$override) {
            $fileTargetPath = $mediaLibraryBasePath . $fileTargetPath;
        }

        $targetPath = dirname($fileTargetPath);
        
        if (!file_exists($targetPath)) {
            $oldmask = umask(0);
            \Wyolution\Logger::debug("Base Path: $mediaLibraryBasePath / Make: $targetPath");
            mkdir($targetPath, 0777, true);
            umask($oldmask);
        }
        \Wyolution\Logger::debug("Resizing file:\n" . $fileSourcePath . ' => ' . $fileTargetPath);
        
        $command = $this->f3->get('magickCommand');
        $command = str_replace(':p1',$fileSourcePath,$command);
        $command = str_replace(':size',$size,$command);
        $command = str_replace(':p2',$fileTargetPath,$command);
        if ($this->f3->get('magickCommandOS') == 'windows') {
            $command = str_replace("'",'"',$command);
        }

        $output = '';
        exec($command,$output);
        \Wyolution\Logger::debug('Conversion command: ' . $command);
        \Wyolution\Logger::debug('Conversion output: ' . print_r($output,true));
        return true;
    }
	    
}