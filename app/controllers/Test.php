<?php

namespace controllers;


/**
 * 
 * A class to test smarty or other things that need testing
 * 
 * @package		controllers
 * @author		Anne Gunn (agunn@sherprog.com)
 * 
 */
class Test extends ControllerPublic {

	// Yes, this data is borrowed from the csf project, but all I need is some sets of values, so why not?
	private $defaultGroups = array('Organization A','Operations','Sales & Marketing','Finance & Admin','IT','R&D', 'Development', 'Customer Support', 'Executives', 'Part-Time', 'Delivery', 'Support Staff', 'Security', 'Maintenance');

	private function getGroups() {
		$f3 = \Base::instance();
		$groups = array();
		$groupCount = count($this->defaultGroups);
		if ($f3->get('GET.groupCount') != '') $groupCount = intval($f3->get('GET.groupCount'));
		for ($i = 0; $i < $groupCount; $i++)
			$groups[] = $this->defaultGroups[$i];
		return $groups;		
	}
	private function getPatterns() {
		$f3 = \Base::instance();
		$patterns = array();
		$patternCount = count($this->defaultPatterns);
		if ($f3->get('GET.patternCount') != '') $patternCount = intval($f3->get('GET.patternCount'));
		for ($i = 0; $i < $patternCount; $i++)
			$patterns[] = $this->defaultPatterns[$i];
		return $patterns;		
	}

	public function smartyTestStep1() {

		$f3 = \Base::instance();
		$f3->set('_announcement','This is a nice little string announcement');
		$f3->set('_testError', 'Uhhh, Houston, we have a problem here');
		$f3->set('_step', 1);

		// Generata sample data, pass it over to the view two ways
		$groups = $this->getGroups();
		// standard f3 -> session (use this only if you need these values to persist!)
		$f3->set('SESSION.groupingOptions',$groups);
		$f3->set('SESSION.selectedGroupIndex', 0);
		// using MT's automagic code in render
		$f3->set('_groupList',$groups);
		$f3->set('_selectedType', 0);

		$this->view->render('test/test.html','layout.html');

	}

	public function smartyTestStep2() {

		$f3 = \Base::instance();
		$f3->set('_announcement','You pushed the button!');
		$f3->set('_testError', 'Uhhh, Houston, we have a problem here. . . . Just kidding!');
		$f3->set('_step', 2);

		$sessionGroupSelectedIndex = $f3->get('POST.sessionGroupType');
		$mtGroupSelectedIndex = $f3->get('POST.mtGroupType');

		// Generate sample data, pass it over to the view two ways
		$groups = $this->getGroups();

		// standard f3 -> session
		$f3->set('SESSION.groupingOptions',$groups);
		$f3->set('SESSION.selectedGroupIndex', $sessionGroupSelectedIndex);
		// using MT's automagic code in render
		$f3->set('_groupList',$groups);
		$f3->set('_mtSelectedGroup',$mtGroupSelectedIndex);

		$this->view->render('test/test.html','layout.html');

	}

	// Devs: Use this route to test any page layouts you are developing
	// before the real code to access the layout is ready.
	public function devLayout() {
		$f3 = \Base::instance();
		$this->view->render('release.notes.html','layout.secure.html');
	}
}
