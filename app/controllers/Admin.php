<?php

namespace controllers;

/**
 * 
 * Controller for administrative functions: 
 * - user CRUD
 * - audit log viewing
 * 
 * @package		controllers
 * 
 */
class Admin extends ControllerSecure {

    private function deflectNonAdmin() {
		if (!$this->permissions->hasAdminAccess()) {
			$this->f3->reroute('/');
			exit(0);
		}
    }
	
	public function showList() {
        $this->deflectNonAdmin();
		$this->f3->set('_userList',array());
		$userMapper = new \DB\SQL\Mapper($this->db,'users');
		$userList = $userMapper->find();
		$this->f3->set('_userList',$userList);
		$this->view->render('admin/users.html','admin/layout.secure.html');
		return;
	}

	public function newUser() {
		$userMapper = new \DB\SQL\Mapper($this->db,'users');
		$this->f3->set('_user',$userMapper->cast());
		$this->f3->set('_userTypes', \PhotoFind\Permissions::USER_ROLES);
		$this->view->render('admin/user.html','admin/layout.secure.html');
	}

	public function showUser() {
        $this->deflectNonAdmin();
		$this->f3->set('_user', array());
		$userId = \Wyolution\F3Helpers::getParam('id', 0);
		$userMapper = new \DB\SQL\Mapper($this->db,'users');
		$userMapper->load(array('id = ?',$userId));
		if (!$userMapper->dry()) {
			$this->f3->set('_user',$userMapper->cast());
			$this->f3->set('_userTypes', \PhotoFind\Permissions::USER_ROLES);
			$this->view->render('admin/user.html','admin/layout.secure.html');
		} else {
			$this->view->messageError("Unable to locate user for userId = $userId.");
			$this->showList();
		}
	}

	public function toggleActive() {
        $this->deflectNonAdmin();
		$userId = \Wyolution\F3Helpers::getParam('id', 0);
		if ($this->f3->get('SESSION.user.userId') != $userId) {
			$userMapper = new \DB\SQL\Mapper($this->db,'users');
			$userMapper->load(array('id = ?',$userId));
			if (!$userMapper->dry()) {
				$msg = '';
				if ($userMapper->active == 1) {
					$userMapper->active = 0;
					$msg = 'User is now inactive';
				} else {
					$userMapper->active = 1;
					$userMapper->attempts = 0;
					$msg = 'User is now active';
				}
				$userMapper->save();
				$this->view->messageSuccess($msg);
			} else {
				$this->view->messageError("Unable to locate user for userId = $userId.");
			}
		} else {
			$this->view->messageError('You cannot change your own active status.');
		}
		$this->showList();
	}

	
	public function addOrUpdateUser($task) {
        $this->deflectNonAdmin();
		$formData = \Wyolution\F3Helpers::getParam('user');

		if (empty($formData)) {
			// First time through. Show the user an empty form with some reasonable defaults
			$formData = $this->initItem(array('name', 'username', 'password', 'passwordConfirm', 'attempts'));
			$formData['active'] = 1;
			$formData['accessLevel'] = 'READ_ONLY';
			$this->f3->set('_user',$formData);
			$this->f3->set('_userTypes', \PhotoFind\Permissions::USER_ROLES);
			$this->view->render('admin/user.html','admin/layout.secure.html');
			// We don't come back.
		} else {
			$userMapper = new \DB\SQL\Mapper($this->db,'users');

			// validate the record, assume success
			$doSaveUserRecord = true;
			// On add, must give us both password and confirmation
			if (empty($formData['id']) && ($formData['password'] == '' || $formData['password'] != $formData['passwordConfirm'])) {
				$this->view->messageError("Passwords do not match.");
				$doSaveUserRecord = false;
			} elseif ($formData['password'] != '') {  // task is update
				// On update, password is optional but must follow rules if it is present
				if ($formData['password'] != $formData['passwordConfirm']) {
					$this->view->messageError("Passwords do not match.");
					$doSaveUserRecord = false;
				}
			}
			if ($doSaveUserRecord && $formData['password'] != '' && strlen($formData['password']) < 3) {
				$this->view->messageError('Please make your password at least three letters long.');
				$doSaveUserRecord = false;
			}
			// NB: The following two tests/messages might be giving away more info than is wise
			// for a public-facing web app. They seem about right for an internal app.
			if (empty($formData['id'])) {
				$userCount = $userMapper->count(array('username = ?', $formData['username']));
				if ($userCount > 0) {
					$this->view->messageError('An account with that email already exists. Please use password recovery.');
					$doSaveUserRecord = false;
				}
			} else {	// task is update
				$userCount = $userMapper->count(array('id <> ? and username = ?',
													$formData['id'],
													$formData['username']));
				if ($userCount > 1) {
					$this->view->messageError('A different account with that email already exists.');
					$doSaveUserRecord = false;
				}

				if ($doSaveUserRecord && $this->f3->get('SESSION.user.userId') == $formData['id']) {
					$userMapper->reset();
					$userMapper->load(array('id = ?', $formData['id']));
					if (($userMapper->active == false) && ($formData['active'] == true)) {
						$this->view->messageError('You cannot change your own active status.');
						$doSaveUserRecord = false;
					}
				}
			}

			if ($doSaveUserRecord && !filter_var($formData['username'], FILTER_VALIDATE_EMAIL)) {
				$this->view->messageError('Please enter a valid email address.');
				$doSaveUserRecord = false;
			}
			
			if ($doSaveUserRecord) {
				if ($formData['password'] != '') {
					$formData['password'] =  md5($formData['password']);
				}
				if (!isset($formData['active'])) {
					$formData['active'] = 0;
				}
				$userMapper->reset();
				if (!empty($formData['id'])) {
					$userMapper->load(array('id = ?',$formData['id']));
				}
				$userMapper->name = $formData['name'];
				$userMapper->username = $formData['username'];
				if (!empty($formData['password'])) {
				    $userMapper->password = $formData['password'];
				}
				$userMapper->accessLevel = $formData['permission'];
				$userMapper->active = (!empty($formData['active']))?1:0;
				$userMapper->save();
				$this->showList();
			} else {
				// show the user screen with message, put back the values we started with
				// except never send back passwords
				$formData['password'] = '';
				$formData['passwordConfirm'] = '';
				$this->f3->set('_user',$formData);
				$this->view->render('admin/user.html','admin/layout.secure.html');
				// We don't come back.
			}
		} 
	}

	public function deleteUser() {
        $this->deflectNonAdmin();
		$userId = \Wyolution\F3Helpers::getParam('id', 0);
		if ($this->f3->get('SESSION.user.userId') != $userId) {
			$userMapper = new \DB\SQL\Mapper($this->db,'users');
			$userMapper->load(array('id = ?',$userId));
			if (!$userMapper->dry()) {
				if ($userMapper->erase()) {
					$this->view->messageSuccess('User deleted.');
				} else {
					$this->view->messageError("Unexpected error deleting userId = $userId.");
				}
			} else {
				$this->view->messageError("Unable to locate user for userId = $userId.");
			}
		} else {
			$this->view->messageError('You cannot delete your own user account.');
		}
		$this->showList();
	}
    public function audit() {
        $this->deflectNonAdmin();
		$accesslevel = $this->f3->get('SESSION.user.accessLevel');
		if ($accesslevel != 'FULL_ACCESS') {
			$this->f3->reroute('/');
		}
	$db = $this->f3->get("DB");
    	$audit = new \Wyolution\Mapper(
    		$db,
			'auditlogs',
			null,			// return all fields
			0
		);
    	$records = $audit->find([],['order'=>'eventTime DESC']);
    	$this->f3->set('_audit',$records);
    	$this->view->render('admin/audit.html','admin/layout.secure.html');
    }
}
