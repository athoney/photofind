<?php

namespace controllers\admin;

/**
 * 
 * Controller for quest functions: 
 * 
 * @package		controllers
 * 
 */
class Quest extends \controllers\ControllerSecure {

	protected $f3;
	protected $view;
	protected $db;
	protected $isAdmin = false;

	/**
	 * initialize controller
	 *
	 * @return void
	 */
	public function __construct() {
	    parent::__construct();
		$this->isAdmin = $this->permissions->hasAdminAccess();
	}
	
	private function deflectNonAdmin() {
		if (!$this->permissions->hasAccountManagerAccess()) {
			$this->f3->reroute('/');
		}
	}
	
	public function showList() {
		$this->deflectNonAdmin();
		$quest = new \PhotoFind\Quest();
    	$this->f3->set('_questList', $quest->getQuests());
		$this->f3->set('_questStatus',\PhotoFind\Constants::QUEST_STATUS_LIST);
		$this->view->render('admin/quest.list.html','admin/layout.secure.html');
	}

	public function showQuest() {
	    $this->deflectNonAdmin();

		$questId = intval(\Wyolution\F3Helpers::getParam('Id', 0));
		$questData = [];
		
		$quest = new \PhotoFind\Quest();
		if ($questId == 0) {
		    $this->f3->set('_quest', $quest->getEmptyQuest());
		    $this->f3->set('_questStatus',\PhotoFind\Constants::QUEST_STATUS_LIST);
			$this->view->render('admin/quest.html','admin/layout.secure.html');
			return;
		}
		else if (!empty($questId)) {
		    $questData = $quest->get($questId);
		}

		if (!empty($questData)) {
			$this->f3->set('_quest',$questData);
		    $this->f3->set('_questStatus',\PhotoFind\Constants::QUEST_STATUS_LIST);
			$this->view->render('admin/quest.html','admin/layout.secure.html');
		} else {
			$this->view->messageError("Unable to locate quest.");
			$this->showList();
		}
	}
	
	private function findUpload(array $files, string $name):string {
	    $matchedFile = '';
	    foreach ($files as $file) {
	        if (strpos($file,$name) !== false) {
	            $matchedFile = $file;
	            break;
	        }
	    }
	    return $matchedFile;
	}
	
	public function deleteQuest(int $questId) {
	    $quest = new \PhotoFind\Quest();
	    if ($quest->delete($questId)) {
            $this->view->jsonFormResponse('ok', 0, 'Quest Deleted Successfully','<a href="' . $this->view->getRelativeUrl() . '/admin-quest" class="btn btn-secondary">Exit</a>');
        }
        else {
            $this->view->jsonFormResponse('nok', 0, 'Quest Not Updated');
        }
	}
	
	private function returnQuestPhotos(int $questId) {
		$photos = new \PhotoFind\photo();
		$this->f3->set('_quest', ['photos' => $photos->getQuestPhotos($questId)]);
		$html = $this->view->renderPartial('admin/quest.photos.html',true);
		return $html;
	}
	
	private function addPhoto(array $questData) {
	    $questId = $questData['Id'];

	    $files = new \PhotoFind\Files();
	    $fileList = $files->getUploadedFile();

	    $questData['questPhoto'] = $this->findUpload($fileList,'questPhoto-pf-');
	    \Wyolution\Logger::debug($questData['questPhoto']);

		$quest = new \PhotoFind\Quest();
		$questData = $quest->addQuestPhoto($questData);

		$this->view->jsonFormResponse('ok', 0, 'Quest Photo Successfully Added',$this->returnQuestPhotos($questId));
		return;
	}
	
	private function deletePhoto(array $questData) {
	    $questId = $questData['Id'];

		$quest = new \PhotoFind\Quest();
		
		if ($quest->deleteQuestPhoto($questData)) {
    		$this->view->jsonFormResponse('ok', 0, 'Quest Photo Successfully Deleted',$this->returnQuestPhotos($questId));
		}
		else {
    		$this->view->jsonFormResponse('nok', 0, 'Unable To Delete Quest Photo',$this->returnQuestPhotos($questId));
		}
	}

	public function updateQuest() {
		if (!$this->permissions->hasAccountManagerAccess()) {
		    $this->view->jsonFormResponse(0, 0, 'Update denied.');
		}
	    $questData = \Wyolution\F3Helpers::getParam('quest',[]);
	    
	    if ($questData['action'] == 'addPhoto') {
	        if (!empty($questData['Id'])) {
	           $this->addPhoto($questData);
	        }
	        else {
                $this->view->jsonFormResponse('nok', 0, 'Unknown Quest. Photo not saved.');
	        }
	        return;
	    }
	    elseif ($questData['action'] == 'deletePhoto') {
	        $this->deletePhoto($questData);
	        return;
	    }
	    
	    if (!empty($questData['Id']) && !empty($questData['delete'])) {
	        $this->deleteQuest($questData['Id']);
	        return;
	    }
	    
	    $files = new \PhotoFind\Files();
	    $fileList = $files->getUploadedFile();
	    $questData['prizePhoto'] = $this->findUpload($fileList,'prizePhoto-pf-');
	    
		// validate data needed is present
		$dataMessage = '';
		if (empty($questData['name'])) {
		    $dataMessage .= 'Name missing.<br />';
		}
		if (empty($questData['description'])) {
		    $dataMessage .= 'Description missing.<br />';
		}
		
		// if data validation errors, send back message
		if (!empty($dataMessage)) {
		    $this->view->jsonFormResponse('nok', 0, $dataMessage);
		    return;
		}
		
	    \Wyolution\Logger::debug($questData['prizePhoto']);
		
		$quest = new \PhotoFind\Quest();
		$questData = $quest->update($questData);
		
		if (empty($questData['Id'])) {
		    $this->view->jsonFormResponse('nok', 0, 'Quest Not Updated');
		}
		else {
		    $this->view->jsonFormResponse('ok', $questData['Id'], 'Quest Updated Successfully');
		}
	}
	
}