<?php

namespace controllers\admin;

/**
 * 
 * Controller for user functions: 
 * 
 * @package		controllers
 * 
 */
class User extends \controllers\ControllerSecure {

	protected $f3;
	protected $view;
	protected $db;
	protected $isAdmin = false;

	/**
	 * initialize controller
	 *
	 * @return void
	 */
	public function __construct() {
	    parent::__construct();
		$this->isAdmin = $this->permissions->hasAdminAccess();
	}
	
	private function deflectNonAdmin() {
		if (!$this->isAdmin) {
			$this->f3->reroute('/');
		}
	}
	
	public function showList() {
		$this->deflectNonAdmin();
		$user = new \PhotoFind\User();
    	$this->f3->set('_userList', $user->getUsers());
		$this->view->render('admin/user.list.html','admin/layout.secure.html');
	}

	public function showUser() {
	    $this->deflectNonAdmin();

		$userId = intval(\Wyolution\F3Helpers::getParam('Id', 0));
		$userData = [];
		
		$user = new \PhotoFind\User();
		if ($userId == 0) {
		    $this->f3->set('_user', $user->getEmptyUser());
		    $this->f3->set('_states', \PhotoFind\Constants::STATES);
		    $this->f3->set('_userTypes',\PhotoFind\Permissions::USER_ROLES);
			$this->view->render('admin/user.html','admin/layout.secure.html');
			return;
		}
		else if (!empty($userId)) {
		    $userData = $user->get($userId);
		}

		if (!empty($userData)) {
			$this->f3->set('_user',$userData);
		    $this->f3->set('_states', \PhotoFind\Constants::STATES);
		    $this->f3->set('_userTypes',\PhotoFind\Permissions::USER_ROLES);
			$this->view->render('admin/user.html','admin/layout.secure.html');
		} else {
			$this->view->messageError("Unable to locate user.");
			$this->showList();
		}
	}

	public function updateUser() {
		if (!$this->isAdmin) {
		    $this->view->jsonFormResponse(0, 0, 'Update denied.');
		}
	    $userData = \Wyolution\F3Helpers::getParam('user',[]);
	    
		// validate data needed is present
		$dataMessage = '';
		if (empty($userData['name'])) {
		    $dataMessage .= 'Name missing.<br />';
		}
		if (empty($userData['email']) || !filter_var($userData['email'], FILTER_VALIDATE_EMAIL)) {
		    $dataMessage .= 'Email missing or invalid.<br />';
		}
		if (empty($userData['accessLevel'])) {
		    $dataMessage .= 'Please specify an access level.<br />';
		}
		if (!empty($userData['password'])) {
		    if (strlen($userData['password']) < 6) {
    		    $dataMessage .= 'Password must be at least 6 characters.<br />';
		    }
		    if ($userData['password'] != $userData['passwordConfirm']) {
    		    $dataMessage .= 'Passwords do not match.<br />';
		    }
		}
		else {
		    unset($userData['password']);
		}
		
		// if data validation errors, send back message
		if (!empty($dataMessage)) {
		    $this->view->jsonFormResponse('nok', 0, $dataMessage);
		    return;
		}
		
		$user = new \PhotoFind\User();
		$userData = $user->update($userData);
		
		if (empty($userData['Id'])) {
		    $this->view->jsonFormResponse('nok', 0, 'User Not Updated');
		}
		else {
		    $this->view->jsonFormResponse('ok', $userData['Id'], 'User Updated Successfully');
		}
	}
	
}