<?php

namespace controllers;

/**
 * 
 * Controller for sponsor administrative functions: 
 * - user CRUD
 * 
 * @package		controllers
 * 
 */
class AdminSponsors extends ControllerSecure {

	protected $f3;
	protected $view;
	protected $db;
	protected $isAdmin = false;
	protected $sponsors;

	/**
	 * initialize controller
	 *
	 * @return void
	 */
	public function __construct() {
	    parent::__construct();
		$this->isAdmin = $this->permissions->hasManagerAccess();
		$this->sponsors = new \PhotoFind\Sponsors();
		//problem here
	}
	
	private function deflectNonAdmin() {
		if (!$this->isAdmin) {
			$this->f3->reroute('/');
		}
	}
	
	public function getSponsors() {
	    $this->deflectNonAdmin();
	    
	    $active = \Wyolution\F3Helpers::getParam('active','');
	    if ($active == '1') {
	        $this->f3->set('SESSION.dashboard.activeSponsors',1);
	    }
	    elseif ($active == '0') {
	        $this->f3->set('SESSION.dashboard.activeSponsors',0);
	    }
	    $active = $this->f3->get('SESSION.dashboard.activeSponsors');
	    
	    if ($active != '0') {
	        $active = 1;
	    }
	    else {
	        $active = 0;
	    }
	    $this->f3->set('SESSION.dashboard.activeSponsors',$active);
	    
		$this->f3->set('_sponsorList', []);
		$this->f3->set('_sponsorList',$this->sponsors->getSponsors($active));
		$this->view->render('admin/sponsors.html','admin/layout.secure.html');
	}
	
	public function getSponsor() {
		$this->deflectNonAdmin();
		$sponsorId = \Wyolution\F3Helpers::getParam('id',0);
		$this->f3->set('_sponsor',$this->sponsors->getSponsor($sponsorId));
		$this->view->render('admin/sponsor.html','admin/layout.secure.html');
	}
	
	public function update() {
		$sponsorData = \Wyolution\F3Helpers::getParam('sponsor',[]);
		
		$sponsorData = $this->sponsors->updateSponsor($sponsorData);

		if (empty($sponsorData['id'])) {
			$this->view->messageFailure('Sponsor Not Updated');
			$this->f3->reroute('/admin/sponsor');
		}
		else {
			$this->view->messageSuccess('Sponsor Updated Successfully');
			$this->f3->reroute('/admin/sponsor/' . $sponsorData['id']);
		}
	}

	public function changeStatus() {
		$sponsorData = \Wyolution\F3Helpers::getParam('sponsor', []);
		if (empty($sponsorData['id'])) {
			$this->view->messageFailure('No sponsor specified.');
		}
		else {		
			$result = $this->sponsors->changeStatus($sponsorData);
			if ($result) {
				$this->view->messageFailure('Unable to update sponsor status.');
			}
			else {
				$this->view->messageFailure('Sponsor status updated.');
			}
		}
		$this->f3->reroute('/admin/sponsors');
	}
}
