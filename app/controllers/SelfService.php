<?php

namespace controllers;


class SelfService extends ControllerPublic {

	public function forgot() {
	   $this->view->render('admin/login.reset.html', 'admin/layout.html');
	   return;
	}
	
	public function requestReset() {
		\Wyolution\Logger::debug("in requestReset");
		$this->view->clearMessages();

		$showRequestForm = true;
		$username = \Wyolution\F3Helpers::getParam('username');
		if (empty($username)) {
		    $this->forgot();
		    return;
		}

        $userMapper = new \DB\SQL\Mapper($this->db, 'user');
        $userMapper->load(array("username = ?", $username));
        if (!$userMapper->dry()) {
            // Create an activation token with a 24-hour expiration
            $authTokenMgr = new \PhotoFind\AuthTokenMgr();
            list($rmInfo, $expiresAt) = $authTokenMgr->generateAndSaveAuthTokenRecord($userMapper->Id,
                $this->f3->get('PASSWORD_RESET_TOKEN_EXPIRE_INTERVAL'),
                $tokenType='PASSWORD_RESET');

            if ($rmInfo != '') {
                \Wyolution\Logger::debug("about to instantiate Mailer");
                // Insert it into an email as a link back to the activation page
                // and send to the user's email address (aka username)
                $mail = new \PhotoFind\MailerWrapper();
                \Wyolution\Logger::debug("back from instantiating Mailer");
                $sentOk = $mail->sendPasswordResetEmail($userMapper->username, $rmInfo);
                if ($sentOk) {
                    $appName = $this->f3->get('_applicationName');
                    $this->view->messageSuccess("Please check your email for a message from $appName. It will contain a link to allow you to reset your password.");
                    \Wyolution\Audit::log('Password reset email sent.', $username);
                    $showRequestForm = false;
                } else {
                    $this->view->messageError("We were unable to send your password reset email. Please contact support.");
                    \Wyolution\Audit::log('Password reset email failed to send.', $username);
                }
            } else {
                $this->view->messageError("We were unable to generate your password reset code. Please contact support.");
                \Wyolution\Audit::log('Password reset could not generate authToken.', $username);
            }
        } else {
            $this->view->messageError("No account is associated with that email. Please try again.");
            \Wyolution\Audit::log('Password reset request for unknown username.', $username);
        }

	    $this->view->render('admin/login.reset.html', 'admin/layout.html');
		return;
	}
	
	public function resetPassword() {
		\Wyolution\Logger::debug("in resetPassword");
		$authToken = null;
		$okToReset = false;

		$formData = \Wyolution\F3Helpers::getParam('user',[]);
		$authToken = \Wyolution\F3Helpers::getParam('token','');

		if (empty($authToken) && !empty($formData['authToken'])) {
		    $authToken = $formData['authToken'];
		}

		if (empty($authToken)) {
	       $this->view->render('admin/login.html', 'admin/layout.html');
	       return;
		}

        $authTokenMgr = new \PhotoFind\AuthTokenMgr();
        $user = $authTokenMgr->getUserFromAuthToken($authToken, $tokenType='PASSWORD_RESET');
        if (!empty($user)) {
            $username = $user['userName'];
            if (!$user['expired']) {
                $okToReset = true;
            }
            else {
                $this->view->messageError("Your request to reset your password has expired. Please try again.");
                \Wyolution\Audit::log('Failed passwordReset, authToken expired.', $username);
            }
        }
        else {
            $this->view->messageError("Unexpected error. Your request to reset your password was not found. Did you already reset it?");
            \Wyolution\Audit::log('Failed passwordReset. AuthToken did not match any stored authToken.', 'unknown');
        }

		if (empty($formData)) {
			// First time here. Need to have a valid, unexpired auth token
			if ($okToReset) {
				$formData = array('authToken' => $authToken);
				$this->f3->set('_user', $formData);
				$this->view->render('admin/login.password.reset.html', 'admin/layout.html');
			} 
			else {
				$this->view->render('admin/login.html', 'admin/layout.html');
			}
		} 
		else {
			if ($okToReset) {
				$doSaveUserRecord = $this->validatePassword($formData);
				if ($doSaveUserRecord) {
					$userMapper = new \DB\SQL\Mapper($this->db, 'user');
					$userMapper->load(['Id = ?', $user['userId']]);
					if (!$userMapper->dry()) {
						// We not only reset the password, we make the user account all fresh and new
						$userMapper->active = 1;
						$userMapper->attempts = 0;
						$userMapper->password = md5($formData['password']);
						$userMapper->save();
						$this->view->messageSuccess("Your password has been reset.<br>Please log in.");
						$authTokenMgr->forgetAuthToken($authToken);
						\Wyolution\Audit::log('Password reset', $username);
					} 
					else {
						$this->view->messageError("Unexpected error. Could not find your user account. Please contact support.");
						\Wyolution\Audit::log('Failed passwordReset. Could not find user account.', $username);
					}
				}
			} 
			if ($this->view->haveErrors()) {
				$formData = ['authToken' => $authToken];
				$this->f3->set('_user', $formData);
				$this->view->render('admin/login.password.reset.html', 'admin/layout.html');
			} else {
				$this->view->render('admin/login.html', 'admin/layout.html');
			}
		}
		return;
	}

	/**
	 * @param $formData
	 * @return bool
	 */
	private function validatePassword($formData) {
		$doSaveUserRecord = true;
		if (strlen($formData['password']) < 6) {
			$this->view->messageError('Please make your password at least six letters long.');
			$doSaveUserRecord = false;
		} else if ($formData['password'] == '' || $formData['password'] != $formData['passwordConfirm']) {
			$this->view->messageError("Passwords do not match.");
			$doSaveUserRecord = false;
		}
		return $doSaveUserRecord;
	}

}
