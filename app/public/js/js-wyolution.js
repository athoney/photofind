;(function($) {
    window.WyoUI = (function()
    {
        var _private =
        {
            webRootPath: '',
            errorList: [],
            apiPath: function() {
                return this.webRootPath;
            },
            request: function(id, method, url, data, dataItem, filesPresent)
            {
                _private.log('in request, Fetching [' + url +']');
                var contentTypeValue = 'application/x-www-form-urlencoded; charset=UTF-8';
                var processDataValue = true;

                // if sending files, set the following
                if (filesPresent) {
                    _private.log('we have files to send, setting contentType and processData to false');
                    // readmore : http://api.jquery.com/jquery.ajax/
                    contentTypeValue = false;
                    processDataValue = false;
                }

                $.ajax({
                        dataType: "json",
                        type: method,
                        url: url,
                        data: data,
                        cache: false,
                        contentType: contentTypeValue,
                        processData: processDataValue,
                        success: function(data){
                            console.log("back from ajax call, in success function");
                            if (data.type == 'undefined') {
                                console.log('Unknown Response');
                                WyoUI.appMessage('alert','Unknown error has occurred.  Please try again later or contact support.');
                            }
                            else if (data.type == 'formResponse') {
                                WyoUI.formResponse(data,id);
                            }
                            else if (data.type == 'html' && id !== '') {
                                $(id).html(data.results.newHtml);
                            }  else if (typeof data.results.init != 'undefined' ) {
                                // Custom javascript for this request wants to handle
                                // both success and failure. Let her.
                                console.log('custom function: ' + data.results.init);
                                var initFunction = new Function('p1', data.results.init);
                                initFunction(data);
                            } else {
                            }
                        },
                    error: function(req, textStatus, errorThrown){
                        console.log("back from ajax call, in failure function");
                        alert('Unable to complete request.  Please try again.');
                        console.log('Request failed: [' + textStatus + ']');
                    }
                });
            },
            log: function(message) {
            if (window.console)
                console.log(message);
            }
        };
        return {
            appMessage: function(type, message) {
            	// TODO Alertify box
            	// alertify.notify(message, type, 5);
            	$('#toastWrapper').append('<div class="toast toast-' + type + '" role="alert" aria-live="assertive" aria-atomic="true" data-autohide="false"><div class="toast-body">' + message + '</div></div>');
    			$('.toast').toast('show');
//                $('#appMessages').html('<div class="alert alert-' + type + ' app-message">' + message + '</div>')
//                setTimeout(function() {$('.app-message').fadeOut('slow');},4000);
            },
            formResponse: function(data,id) {
                this.appMessage((data.status == 'ok')?'success':'danger',data.message);
                if (data.status == 'ok') {
                    if (data.id != 'undefined' && data.id != '0') {
                        $('#itemId').val(data.id);
                    }
                    if (data.html != '' && id != '') {
                        $(id).html(data.html);
                    }
                }
            },
            get: function(path,elementId)
            {
                _private.request('#'+elementId,'GET', _private.apiPath() + path, null, null, false);
                return false;
            },
            getData: function(path,elementId,data)
            {
                var encodedData = $.param( data );
                _private.request('#'+elementId,'GET', _private.apiPath(), encodedData, null, false);
                return false;
            },
            // Inquiring minds often need to be reminded of why 
            // post uses .serialize() and postFiles uses FormData.
            // Succinct answer here.
            // http://stackoverflow.com/questions/33469684/formdata-vs-serialize-what-is-the-difference
            // At this point in IE history, we probably should just use FormData both places.
            post: function(formId, elementId, path)
            {
                if (path == '') {
                    path = $('#' + formId).attr('action');
                }
                var formData = $('#'+formId).serialize();
                var localElementId = '';
                if (elementId !== '') {
                    localElementId = '#'+elementId;
                }
                _private.request(localElementId,'POST', path, formData, null,false);
                return false;
            },
            postFiles: function(formId, elementId, path)
            {
                if (path == '') {
                    path = $('#' + formId).attr('action');
                }
                _private.log("in postFiles, path = " + path + ", formId = " + formId);
                var formElement = $('#'+formId)[0];
                // Serialize the fields and files from the form
                var formData = new FormData(formElement);

                var localElementId = '';
                if (elementId !== '') {
                    localElementId = '#'+elementId;
                }

                _private.request(localElementId,'POST', path, formData, null, true);
                _private.log("postFiles request made");
                return false;
            },
            confirm: function(msg) {
                var r = confirm(msg);
                return r;
            },
            hideMessage: function() {
                if ($('.message').length > 0) {
                    setTimeout(function(){$('.message').slideToggle();},3000);
                }
            },
            showPreview: function(imageInput) {
                if (imageInput.files && imageInput.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#'+imageInput.id+'-preview').attr('src', e.target.result).slideDown();
                    }

                    reader.readAsDataURL(imageInput.files[0]);
                }
            },
            getWebRootPath: function() {
                return _private.webRootPath;
            },
            init: function(path)
            {
                _private.webRootPath = path;
            }
        };
    })();
})(jQuery);


$(document).ready(function() {
    WyoUI.init(appConfig.appPath);
    console.log("js-wyolution.js after init");
});


