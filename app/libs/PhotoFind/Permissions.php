<?php


namespace PhotoFind;


class Permissions
{
    const USER_ROLES = [
            'admin'          => 'Administrator',
            'accountmanager' => 'Account Manager',
            'accountuser'    => 'Account User',
            'user'           => 'User'
    ];
    const USER_ROLE_ADMIN           = 'admin';
    const USER_ROLE_ACCOUNT_MANAGER = 'accountmanager';
    const USER_ROLE_ACCOUNT_USER    = 'accountuser';
    const USER_ROLE_USER            = 'user';
    
    protected $f3;
    protected $db;
    
    public function __construct()
    {
        $this->f3 = \Base::instance();
        $this->db = $this->f3->get("DB");
    }
   
    public function userRoleList():array {
        return $this->hasAdminAccess() ? self::USER_ROLES:[];
    }
    public function getRole():string {
        return trim($this->f3->get('SESSION.user.accessLevel'));
    }
    public function getRoleDesc($role=''):string {
        return $this::USER_ROLES[$role]??'Unknown';
    }
    public function getUserId():int {
        return intval($this->f3->get('SESSION.user.Id'));
    }
    public function getUserInfo():string {
        $info = $this->f3->get('SESSION.user.name');
        $info .= ' (' . $this->f3->get('SESSION.user.accessLevelLabel') . ')';
        return $info;
    }
    
    public function hasAdminAccess($role = '') {
        $role = empty($role) ? $this->getRole() : $role;
        return  self::USER_ROLE_ADMIN == $role;
    }
    public function isAdmin() {
        return self::USER_ROLE_ADMIN == $this->getRole();
    }

    public function hasAccountManagerAccess($role = '') {
        $role = empty($role) ? $this->getRole() : $role;
        return self::USER_ROLE_ACCOUNT_MANAGER == $role || $this->hasAdminAccess();
    }
    public function isAccountManager() {
        return self::USER_ROLE_ACCOUNT_MANAGER == $this->getRole();
    }

    public function hasAccountUserAccess($role = '') {
        $role = empty($role) ? $this->getRole() : $role;
        return self::USER_ROLE_ACCOUNT_USER == $role || $this->hasAccountManagerAccess();
    }
    public function isAccountUser() {
        return self::USER_ROLE_ACCOUNT_USER == $this->getRole();
    }

    public function hasUserAccess($role = '') {
        $role = empty($role) ? $this->getRole() : $role;
        return $this->hasAdminAccess($role) ||
            $this->hasManagerAccess($role) ||
            (self::USER_ROLE_USER == $role);
    }
    public function isUser() {
        return self::USER_ROLE_USER == $this->getRole();
    }

}