<?php


namespace PhotoFind;


class ModelBase
{
	protected $f3;
	protected $db;
	protected $permissions;
	protected $statusMessages;
	
	public function __construct()
	{
		$this->f3 = \Base::instance();
		$this->db = $this->f3->get("DB");
		$this->permissions = new \PhotoFind\Permissions();
	}
	
	static protected function convertToHash(\DB\SQL\Mapper $mapper) {
	    $results = [];
	    while (!$mapper->dry()) {
	        $results[] = $mapper->cast();
	        $mapper->next();
	    }
	    return $results;
	}
	
	protected function hashDiff(array $set1, array $set2, array $filter): array {
	    if(!empty($filter)) {
	        $keys = array_keys($set1);
	        
	        foreach($keys as $key) {
	            if(!in_array($key, $filter)) {
	                unset($set1[$key]);
	            }
	        }
	    }
	    echo(print_r(array_diff_assoc($set1, $set2), true));
	    
	    return array_diff_assoc($set1, $set2);
	}
	
	static function getRandomString(int $length):string {
        $permitted_chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $randomString = substr(str_shuffle($permitted_chars), 0, $length);
    
	    return $randomString;
	}

}