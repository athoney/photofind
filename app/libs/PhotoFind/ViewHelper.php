<?php


namespace PhotoFind;


/**
 * Class ViewHelper -- this is the one and only class that \Wyolution\View::render will register
 * with Smarty just before rendering our layouts. It contains methods for:
 * - giving the views access to specific data sets needed for drop downs, etc
 * - concatenating or formatting data in ways that are easier or better done in php code than in smarty
 */
class ViewHelper extends ModelBase
{

	public function __construct()
	{
		parent::__construct();
	}

	public function getAccessLabel($accessLevel){
		return \PhotoFind\Constants::USER_TYPE_VALUE_LABEL[$accessLevel]??'unknown';
	}
	
	public function formatLineItemAmount($amount, $unit) {
		$formattedAmount = '';
		if (!empty($amount)) {
			$amount = intval($amount);
		} else {
			$amount = 0;
		}
		if ($unit == 'Money') {
			$formattedAmount = '$' . number_format($amount, 2);
		}  else {
			$formattedAmount = number_format($amount);
		}
		return $formattedAmount;
	}
}