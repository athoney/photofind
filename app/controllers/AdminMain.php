<?php

namespace controllers;

/**
 *
 * @package controllers
 *        
 */
class AdminMain extends ControllerSecure {
	
	/**
	 * Get assets needed to render homepage
	 *
	 * @return void
	 */
	public function dashboard() {
		$this->view->render('admin/dashboard.html','admin/layout.secure.html');
	}
}