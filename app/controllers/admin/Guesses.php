<?php

namespace controllers\admin;

/**
 * 
 * Controller for sponsor administrative functions: 
 * - user CRUD
 * 
 * @package		controllers
 * 
 */
class Guesses extends \controllers\ControllerSecure {

	protected $f3;
	protected $view;
	protected $db;
	protected $isAdmin = false;
	protected $guesses;
	protected $photos;
	
	const MATCH      = 2;
	const NOREVIEWED = 1;
	const INCORRECT  = 0;

	/**
	 * initialize controller
	 *
	 * @return void
	 */
	public function __construct() {
	    parent::__construct();
		$this->isAdmin = $this->permissions->hasAccountManagerAccess();
		$this->guesses = new \PhotoFind\Guesses();
		$this->photos = new \PhotoFind\Photos();
		
		//problem here
	}
	
	private function deflectNonAdmin() {
		if (!$this->isAdmin) {
			$this->f3->reroute('/');
		}
	}
	
	public function getGuesses() {
	    $this->deflectNonAdmin();
	    $photoId = \Wyolution\F3Helpers::getParam('Id',0);
	    $guessList = $this->guesses->getGuesses($photoId);
	    
 	    $this->f3->set('_guessList', $guessList);
 	    $this->f3->set('_photo', $this->photos->getPhoto($photoId));
	    //$this->f3->set('_prizeList',$this->prizes->getPrizes);
	    $this->view->render('admin/guesses.1.html','admin/layout.secure.html');
	}
	
	public function getQuests(){
	    $quest = new \PhotoFind\Quest();
	    $questList = $quest->getQuests();
	    $questDetail = [];
	    foreach ($questList as $questInfo) {
	        if ($questInfo['status'] != 'draft') {
	           $questDetail[] = $quest->get($questInfo['Id']);
	        }
	    }
	    $this->f3->set('_questList', $questDetail);
	    $this->view->render('admin/guesses.quests.html','admin/layout.secure.html');
	}
	
	public function getQuestPhotos(){
	    $questId = \Wyolution\F3Helpers::getParam('questId',0);
	    $quest = new \PhotoFind\Quest();
	    $questDetail = $quest->get($questId);
	    $leaderboard = $quest->getLeaderboard($questId,count($questDetail['photos']['questphoto']));
	    $this->f3->set('_quest', $questDetail);
	    $this->f3->set('_leaderboard',$leaderboard);
	    $this->view->render('admin/guesses.quest.photos.html','admin/layout.secure.html');
	}
	
	public function getPhotoGuesses(){
	    $photoId = \Wyolution\F3Helpers::getParam('photoId',0);
	    $quest = new \PhotoFind\Quest();
	    $questId = $quest::getQuestIdByPhotoId($photoId);
	    
	    if (!$quest->hasAccessToQuest($questId)) {
	        $this->f3->reroute('/dashboard');
	        return;
	    }

	    $guesses = new \PhotoFind\Guesses();
    	$allGuesses = $guesses->getGuesses($questId, $photoId);
	    $this->f3->set('_guesses',$allGuesses);
	    $this->view->render('admin/guesses.html','admin/layout.secure.html');
	    
	}

	/*
		$this->f3->set('SESSION.dashboard.activeSponsors',$active);
	    
		$this->f3->set('_sponsorList', []);
		$this->f3->set('_sponsorList',$this->sponsors->getSponsors($active));
		$this->view->render('admin/sponsors.html','admin/layout.secure.html');
	*/
	
	public function getPhotos() {
	    $this->deflectNonAdmin();
	    
	    $active = \Wyolution\F3Helpers::getParam('active','');
	    if ($active == '1') {
	        $this->f3->set('SESSION.dashboard.activePhotos',1);
	    }
	    elseif ($active == '0') {
	        $this->f3->set('SESSION.dashboard.activePhotos',0);
	    }
	    $active = $this->f3->get('SESSION.dashboard.activePhotos');
	    
	    if ($active != '0') {
	        $active = 1;
	    }
	    else {
	        $active = 0;
	    }
	    $this->f3->set('SESSION.dashboard.activePhotos',$active);
	    
		$this->f3->set('_photoList', []);
		$this->f3->set('_photoList',$this->photos->getPhotos($active));
		$this->view->render('admin/guesses.html','admin/layout.secure.html');
	}
	
	public function getGuess() {
		$this->deflectNonAdmin();
		$guessId = \Wyolution\F3Helpers::getParam('id',0);
		$this->f3->set('_guess',$this->guesses->getGuess($guessId));
		$this->view->render('admin/guess.html','admin/layout.secure.html');
	}
	
	public function update() {
		$guessData = \Wyolution\F3Helpers::getParam('guess',[]);
		
		$errorMessage = '';
		if (empty($guessData['questId'])) {
			$errorMessage = 'Unknown quest.<br />';
		}
		if (empty($guessData['guessId'])) {
			$errorMessage = 'Unknown guess.<br />';
		}
		if (empty($guessData['photoId'])) {
			$errorMessage = 'Unknown photo.<br />';
		}
		if (empty($guessData['action']) || !in_array($guessData['action'],['match','incorrect','delete'])) {
			$errorMessage = 'Unknown request.<br />';
		}
		if (!empty($errorMessage)) {
		    $this->view->jsonFormResponse('nok', 0, $errorMessage);
		    return;
		}
		
		$quest = new \PhotoFind\Quest();
		if (!$quest->hasAccessToQuest($guessData['questId'])) {
		    $this->view->jsonFormResponse('nok', 0, 'Update failed.');
		    return;
		}
		
		$guesses = new \PhotoFind\Guesses();
		if ($guessData['action'] == 'match') {
		    // mark match
		    $guesses->updateMatch($guessData['guessId'], self::MATCH);
		}
		else if ($guessData['action'] == 'delete') {
		    // delete record and associated file
		   $guesses->deleteGuess($guessData['guessId']);
		}
		else {
		    // mark incorrect
		    $guesses->updateMatch($guessData['guessId'], self::INCORRECT);
		}

		$allGuesses = $guesses->getGuesses($guessData['questId'],$guessData['photoId']);
		//echo 'photo id: ' . $guessData['photoId'] . "\n";
		//echo print_r($allGuesses,true) . "\n";
		//exit(0);
		/*
		if ($allGuesses['Id'] != $guessData['questId']) {
		    $this->view->jsonFormResponse('nok', 0, 'Load error.');
		    return;
		}
		*/

		$this->f3->set('_guesses', $allGuesses);
		$html = $this->view->renderPartial('admin/guesses.list.html',true);
		$this->view->jsonFormResponse('ok', 0, 'Update Successful.',$html);
		return;
	}
	
	public function updateWinner() {
		$questId = intval(\Wyolution\F3Helpers::getParam('questId', 0));
		$status  = intval(\Wyolution\F3Helpers::getParam('status', 0));
		$prize  = intval(\Wyolution\F3Helpers::getParam('prize', 0));
		$email  = \Wyolution\F3Helpers::getParam('email', '');
		$action  = \Wyolution\F3Helpers::getParam('action', '');

		if (empty($questId) || empty($email) || !in_array($action,['winner','prize'])) {
		  $this->view->jsonFormResponse('nok', 0, "Request failed.");
		  return;
		}
		
		$quest = new \PhotoFind\Quest();
		if (!$quest->hasAccessToQuest($questId)) {
		  $this->view->jsonFormResponse('nok', 0, 'Access denied.');
		}
		
		$winner = new \PhotoFind\Winner();
		if ($action == 'prize') {
		    if ($prize == 1) {
		        $winner->updatePrize($questId, $email, 0);
		    }
		    else {
		        $winner->updatePrize($questId, $email, 1);
		    }
		}
		else {
            if ($status == 0) {
                // toggle winner on
                $winner->addWinner($questId, $email);
            }
            else {
                // toggle winner off
                $winner->removeWinner($questId, $email);
            }
		}
		
		$questDetail = $quest->get($questId);
		$leaderboard = $quest->getLeaderboard($questId,count($questDetail['photos']['questphoto']));
		$this->f3->set('_quest', $questDetail);
		$this->f3->set('_leaderboard',$leaderboard);
		$html = $this->view->renderPartial('admin/guesses.quest.photos.leaderboard.html',true);
		$this->view->jsonFormResponse('ok', 0, 'Winner updated.',$html);
	}

}
