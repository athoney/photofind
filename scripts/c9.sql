-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `auditlogs`;
CREATE TABLE `auditlogs` (
  `auditId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `eventTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(45) NOT NULL DEFAULT '',
  `info` text NOT NULL,
  PRIMARY KEY (`auditId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `auth_tokens`;
CREATE TABLE `auth_tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `userId` int(10) unsigned NOT NULL,
  `selector` varchar(64) NOT NULL,
  `validator` varchar(256) NOT NULL,
  `expires` datetime NOT NULL,
  `tokenType` enum('LOGIN_COOKIE','ACTIVATE','PASSWORD_RESET') NOT NULL DEFAULT 'LOGIN_COOKIE',
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_tokens_userId_index` (`userId`),
  UNIQUE KEY `auth_tokens_selector_index` (`selector`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `guesses`;
CREATE TABLE `guesses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `email` varchar(128) NOT NULL,
  `fileLocation` varchar(2000) NOT NULL,
  `submitDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `photos`;
CREATE TABLE `photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `dateUploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `fileLocation` varchar(2000) NOT NULL,
  `hint1` varchar(2000) DEFAULT NULL,
  `hint1Date` timestamp NULL DEFAULT NULL,
  `hint2` varchar(2000) DEFAULT NULL,
  `hint2Date` timestamp NULL DEFAULT NULL,
  `hint3` varchar(2000) DEFAULT NULL,
  `hint3Date` timestamp NULL DEFAULT NULL,
  `winnerId` int(11) DEFAULT NULL,
  `prizeId` int(11) DEFAULT NULL,
  `sponsorId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `prize`;
CREATE TABLE `prize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `photo` varchar(2000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE `sponsors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` text,
  `logo` varchar(2000) DEFAULT NULL,
  `pocName` varchar(255) DEFAULT NULL,
  `pocPhone` varchar(255) DEFAULT NULL,
  `pocEmail` varchar(255) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(96) NOT NULL,
  `password` varchar(255) NOT NULL,
  `accessLevel` varchar(255) NOT NULL,
  `active` int(1) NOT NULL DEFAULT '1',
  `attempts` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `name`, `username`, `password`, `accessLevel`, `active`, `attempts`) VALUES
(1,	'Test Admin',	'test@thoney.net',	'e77989ed21758e78331b20e477fc5582',	'ADMINISTRATOR',	1,	0),
(3,	'Alicia',	'alicia.thoney@gmail.com',	'e77989ed21758e78331b20e477fc5582',	'ADMINISTRATOR',	1,	0);

-- 2018-08-14 17:48:35
