$(document).ready(function() {
    
    $('.focus').first().focus();
    
    //$.fn.dataTable.moment( 'MMM DD, YYYY' );
    
	$('table.data-table').DataTable({
		language: {
			search: "Filter:"
		},
		stateSave: true,
		fixedHeader: true,
		responsive: false,
		paging: false,
		order: [[ 0, "asc" ]],
		columnDefs: [ 
			{
				targets: 'no-sort',
				orderable: false
			},
			{ 
				visible: false,
				searchable: false,
				targets: 'hidden-col'
			}
		]
	});

	$('table.data-table-paged').DataTable({
		language: {
			search: "Filter:"
		},
		stateSave: true,
		fixedHeader: true,
		scrollX: true,
		paging: true,
		order: [],
		columnDefs: [ 
			{
				orderable: false,
				targets: 'no-sort'
			},
			{ 
				visible: false,
				searchable: false,
				targets: 'hidden-col'
			}
		]
	});    
	
	$('#toastWrapper').on('show.bs.toast', function (e) {
		//console.log(e, $(e.target).attr('id'));
		if ($(e.target).attr('id')) {
			return;
		}
		var targetId = 'toast-' + new Date().getTime();
		//console.log('test', targetId);
		$(e.target).attr('id', targetId);
		setTimeout(function(){
			removeToast(targetId);
		}, 3000);
        var top = 10 + $(window).scrollTop();
        $('#toastWrapper').attr('style','position: absolute; top: ' + top + 'px; right: 5px;');
		//$('#toastWrapper').show();
	});
	
	function removeToast(targetId) {
		//console.log('remove', targetId);
		$('#' + targetId).slideUp("slow", "", function() {
			$('#' + targetId).remove();
			if ($('#toastWrapper div').length == 0) {
				$('#toastWrapper').hide();
			}
		});			
	}
});